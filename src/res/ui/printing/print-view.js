import {decode} from "@sciter";

export class PrintView extends Element {

  note;
  printers = [];
  numPages = 1;
  curPage = 1;

  constructor({note}) {
    super();
    this.note = note;
  }

  render() {
    return <section styleset={__DIR__ + "print-view.css#print-view"}>
      <aside>
        <header>Printer:</header>
        <form #printers>{ 
             this.printers.map( printer => 
               <button|radio(printer) value={printer.id} state-checked={printer.isDefault}>{printer.name}</button> )}
        </form>
        <button #print>Print</button>
      </aside>
      <div>
        <header><span>Page</span><input|integer #current-page value={this.curPage} step="1" min="1" max={this.numPages} />
                <span>of pages:</span><output|integer #numpages value={this.numPages}/>
                <button #close />
        </header>
        <frame|pager(pager) cols="1" page-template={ __DIR__ + "page-template.htm" } content-style={ __DIR__ + "print.css" } />
      </div>
    </section>;
  }

  componentDidMount() {
    this.componentUpdate({printers:this.pager.printers()});
    this.$("frame|pager").onrequest = rq => { 
      if( this.note )
        this.note.onrequest(rq);
    }; 
  }

  get pager() {
    return this.$("frame|pager").pager;
  }

  // template is loaded, page size is known - pager is ready to accept document:
  ["on paginationready"](evt) {
    this.pager.loadHtml(decode(this.note.html),this.note.url);
    return true;
  }

  ["on paginationend"](evt) {
    this.componentUpdate({ numPages: evt.reason });
    //document.$("#numpages").value = evt.reason;
    //document.$("#current-page").attributes["max"] = evt.reason;
    return true;
  }

  ["on change at form#printers"](evt,form) { 
    this.pager.selectPrinter( form.value.printer ); 
    return true;
  }

  ["on click at button#print"]() { 
    this.pager.print(); 
    return true;
  }

  ["on change at #current-page"](evt,pageNo) { 
    this.pager.page = pageNo.value;
    this.componentUpdate({curPage:pageNo.value});
    return true;
  }

  ["on click at button#close"]() {
    document.body.state.disabled = false;
    this.remove();
  }

  static show(note) {
    document.body.state.disabled = true;
    document.append(<PrintView note={note}/>);
  }

}

