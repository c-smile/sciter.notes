
import * as db from "../db/db.js";

import {addNoteToLRU} from "./lru.js";

export class EventsView extends Element {
  formatter;
  params;

  eventsMap;
  today;
  year;
  todayValue;

  constructor() {
    super();
    this.today = new Date();    
    this.todayValue = this.today.valueOf();
    this.year = this.today.getFullYear();
    this.eventsMap = db.eventsForYear(this.year);
    this.formatter = new Intl.DateTimeFormat();

    const dayClass = (day,month,year) => {
      const key = `${year}-${month}-${day}`;
      const dateValue = (new Date(year,month,day)).valueOf();
      const list = this.eventsMap.get(key);
      if(list) {
        let warning = "";
        let past = "";
        for(const note of list) {
          if( note.startDate.valueOf() < this.todayValue )
            warning = ".warning";
          if( note.endDate.valueOf() < this.todayValue )
            past = ".past";
        }
        return ".events" + warning + past;
      }
      return "";
    };

    this.params = {
       today:this.today, 
       showMonth:true,
       showWeekDays:true,
       dayOfWeekLength:2, // day abbr length
       // custom classes for days
       dayClass,
    }
  }
  render() {
    const months = [];
    const params = this.params;
    const formatter = this.formatter;
    for(let month = 0; month < 12; ++month) {
       // convert HTML-in-string to VDOM by eval() the string:
       const vdom = eval(formatter.monthView(this.year,month,params));
       months.push(vdom);
    }
    return <section.events styleset={__DIR__ + "eventsview.css#events-view"}>
      <toolbar.content> 
        <button.previous />
        <span.year>{this.year}</span>
        <button.next />
      </toolbar>
      <main>
        {months}
      </main>
    </section>;
  }

  ["on click at button.next"]() {
    this.year += 1;
    this.eventsMap = db.eventsForYear(this.year);
    this.componentUpdate();
  }
  ["on click at button.previous"]() {
    this.year -= 1;
    this.eventsMap = db.eventsForYear(this.year);
    this.componentUpdate();
  }

  ["on click at td.day.events"](e,td) {
    const [y,m,d] = scanf("%d-%d-%d",td.attributes["date"]);
    const list = this.eventsMap.get(`${y}-${m-1}-${d}`);
    for(const note of list) 
      addNoteToLRU(note);
  }
}
