
import * as env from "@env";
import * as config from "../../config.js"; 

const SS = CSS.set`
  :root {
    background:linear-gradient(to bottom,color(accent-lite),color(accent));
    size:*;
  }
  :root > section {
    background: color(widget-back);
    margin:*;
    size:max-content;
    max-width:50%;
    padding:1em 2em;
    box-shadow: 2px 4px 5px rgba(0,0,48,0.5);
  }
  button|radio {
    display:block;
    width:*;
    height:max-content;
    white-space:normal;
    padding-left:2em;
  }
  button|radio::marker {
    size:1.4em;
    border-radius:1em;
    margin:1.5em * * 0;
  } 

  button#go {
    display:block;
    margin:0 *;
  }

  header {
    flow: grid(1 2,
               1 3);
  } 

  header > h2,
  header > p { width:*; }
  header > img { margin:* 1em; size:84px; }

`;

export class InitialView extends Element {
  location;
  customDbLocation;

  render() {
    let canCreate = !!this.location;
    if (this.location == "custom") 
        canCreate = !!this.customDbLocation; 
    return <main styleset={SS}>
      <section>
        <header>
          <img src={__DIR__ + "../images/mascot.svg"}/>
          <h2><span @>Welcome aboard</span>, {env.userName()}!</h2>
          <p @>Your notes database {config.DBNAME} is not found at its usual locations therefore please choose where you want it to be created:</p>
        </header>          
        <form>
          <button|radio(location) value="portable">
            <h3>Portable location</h3>
            <p>Beside to the executable.</p>
            <p>Use this location if you plan to store your notes on removable drive (memory card or USB drive). In this case you shall place Sciter.Notes executable on that drive too, sciter-notes.db will be placed in the same folder.</p>
          </button>
          <button|radio(location) value="documents">
            <h3>Permanent location</h3>
            <p>In your <em>Documents</em> folder.</p>
            <p>This location if you plan to store your notes on removable drive (memory card or USB drive). In this case you shall place Sciter.Notes executable on that drive too.</p>
          </button>
          <button|radio(location) value="custom">
            <h3>Custom location</h3>
            <p>Location of your choice.</p>
            <p>Database will be stored at {this.customDbLocation || "(choose)"}.</p>
          </button>
        </form>
        <button#go.primary disabled={!canCreate} @>Create</button>
      </section>
    </main>;
  }

  ["on change at form"](e,form) {
    if(e.target !== form) return;
    let location = form.value.location;
    let customDbLocation;
    if(location === "custom") {
      const url = Window.this.selectFolder();
      if(url)
        customDbLocation = URL.toPath(url);
    }
    this.componentUpdate({location,customDbLocation});
    return true;
  }

  ["on click at button#go"]() {

    let settingsPath;
    let dbPath;
    switch(this.location) {
      case "portable": settingsPath = env.home(config.SETTINGS); 
                       dbPath = env.home(config.DBNAME); 
                       break; 
      case "documents":settingsPath = env.path("appdata",config.SETTINGS); 
                       dbPath = env.path("documents",config.DBNAME); 
                       break; 
      case "custom":   settingsPath = env.path("appdata",config.SETTINGS); 
                       dbPath = this.customDbLocation; 
                       break; 
      default: return true; 
    }
    this.parentElement.onGotDataPaths(settingsPath,dbPath); 
  }

  componentDidMount() {
    FlyingNotesOn(this);
  }
}

function FlyingNotesOn(element) 
{

  // Create an array to store our particles
  const particles = [];

  const colors = [ new Color("#facaec"), new Color("#cdfcb8"), new Color("#fcfcb8") ];
  
  //var back = color("#ccc");

  const particleMaxSize = 120;
  const particleMinSize = 30;

  // The amount of particles to render
  const particleCount = 40;

  const minOpacity = 20;
  const maxOpacity = 200;

  const back = Color.rgb(0.8,0.9,1.0);


  // Set the dimensions of the canvas as variables so they can be used.
  let {left:x1,top:y1,right:x2,bottom:y2} = element.box("border");

  function random( min, max ) { return Math.random() * (max - min) + min; }
  
  function createParticle() 
  {
    let distance = random(0.1,1);
    let size = distance * particleMaxSize;
    let clr = colors[random(0,colors.length) | 0];
    clr = Color.morph(back, clr, distance );

    return {
      x : random(x1, x2),
      y : random(y1, y2),
      a : random(-1, 1) * 0.01,
      xVelocity: random(-0.5, 0.5) * distance,
      yVelocity: random(-0.2, 0.2) * distance,
      aVelocity: random(-2, 2) * 0.001,
      color: clr,
      size: size,
    };
  }

  //gfx.rectangle( 0, 0, dim, dim);          
  
  function moveParticle(particle) 
  {
    // Update the position of the particle with the addition of the velocity.
    particle.x += particle.xVelocity;
    particle.y += particle.yVelocity;
    particle.a += particle.aVelocity;

    // Check if has crossed the right edge
    if (particle.x >= x2) {
        particle.xVelocity = -particle.xVelocity;
        particle.x = x2;
    }
    // Check if has crossed the left edge
    else if (particle.x <= x1) {
        particle.xVelocity = -particle.xVelocity;
        particle.x = x1;
    }

    // Check if has crossed the bottom edge
    if (particle.y >= y2) {
        particle.yVelocity = -particle.yVelocity;
        particle.y = y2;
    }
    
    // check if has crossed the top edge
    else if (particle.y <= y1) {
        particle.yVelocity = -particle.yVelocity;
        particle.y = y1;
    }
    
  }
  
  function drawParticle(gfx,particle)
  {
    gfx.save();              
    gfx.fillStyle = particle.color;
    gfx.translate(particle.x,particle.y);
    gfx.rotate(particle.a);
    gfx.fillRect( -particle.size / 2, -particle.size / 2, particle.size, particle.size);
    gfx.restore();              
  }
  
  element.paintContent = function(gfx) {
    for( let particle of particles )     
      drawParticle(gfx,particle);
  }
  
  // Initialize the scene
  for( let n = 0; n < particleCount; ++n )
    particles.push(createParticle());

  particles.sort( (a,b) => a.size - b.size);

  function step() {
    ({left:x1,top:y1,right:x2,bottom:y2} = element.box("border"));
    
    for( let particle of particles )
      moveParticle(particle);

    element.requestPaint();
    requestAnimationFrame(step);
  }

  requestAnimationFrame(step);

}
