import {VERSION} from "../../config.js";

import * as Settings from "settings.js";

async function checkVersion() {
  try {
    //console.log("checkVersion");
    const rs = await fetch("https://notes.sciter.com/version.json");
    const data = await rs.json();
    if((typeof data.version == "number") && (data.version > VERSION)) {
       Window.this.modal(<info>
         <p>New version {data.version} is available.</p>
         <p>You can download it from <a target="system" href="https://notes.sciter.com">notes.sciter.com</a>.</p>
       </info>);
    }
  } catch(e) {
    console.error(e,e.stack);
  }
}


// logic to make version check once per day only:

let checked; // doy number

function dayNo(date) { return Math.floor(date.valueOf() / 86400000); }

Settings.add({
  store(data) {
    data.versionCheck = checked;
  },
  restore(data) {
    const dno = dayNo(new Date());
    if( data.versionCheck === dno ) return;
    checked = dno;
    Settings.saveState();
    document.timer(1000, ()=>{ checkVersion() });
  }
});


