
class Pie extends Element {
  #value = 0; 
  strokeWidth;
  circleColor;
  strokeColor;
  startAngle = -90deg;

  this({progress}) {
    this.#value = Math.min(progress,1.0);
    this.innerText = printf("%d%%", this.#value * 100);
  }

  render() {
    return <div.pie>
      <text>{printf("%d%%", this.#value * 100)}</text>
    </div>
  }

  componentDidMount() {
    this.strokeWidth = 10px; 
    this.circleColor = Color("#ffffe8"); 
    this.strokeColor = Color("#d4d491"); 
    this.paintContent = this.paint;
    this.value = 0.0;
  }

  paint(gfx) 
  {
    let [w,h] = this.state.box("dimension"); 
    let x = w/2, y = h/2;  
    let r = Math.min(w,h) / 2 - this.strokeWidth / 2;  
    
    gfx.strokeWidth = this.strokeWidth;
    gfx.strokeStyle = this.circleColor;
    gfx.beginPath();
    gfx.arc(x,y,r,0, 360deg);
    gfx.stroke();
    gfx.beginPath();
    gfx.strokeStyle = this.strokeColor;
    gfx.arc(x,y,r,this.startAngle, this.startAngle + 360deg * this.#value);
    gfx.stroke();
  }  

  // redefining property 'value' on that element
  get value() { return this.#value; }
  set value(v) {
    this.#value = Math.min(v,1.0);
    this.componentUpdate();
  }
}

const SS = CSS.set`
  section.cover {
    position:fixed;
    size:*;
    top:0;
    left:0;
    background:rgba(255,255,255,0.5);
  }

  section.cover > div {
    width:20rem;
    height:21rem;
    margin:*;
    background:#fcfcb8;
    box-shadow: 2px 2px 4px #666; 
  }

  section.cover > div > caption {
    text-align:center;
    line-height:2em;
  }

  div.pie {
    size:*;
    margin:1em;
  }
  div.pie > text {
    margin:* 0;
    text-align:center;
    font-weight:bold;
    font-size:18px;
  }
`;

export class Progress extends Element {
  #progress = 0;
  caption;

  this({progress,caption}) {
    this.#progress = progress;
    this.caption = caption;
  }

  render() {
    return <section.cover styleset={SS}>
      <div>
        <caption>{this.caption}</caption>
        <Pie progress={this.#progress}/>
      </div>
    </section>;
  }

  get progress() { return this.#progress; }
  set progress(v) {
    if(v != this.#progress) {
      this.#progress = v; 
      this.patch(this.render());
      this.requestPaint();
    }
  }

  async shutdown() {
    const step = progress => { this.style.opacity = 1.0 - progress; return true;}
    await this.morphContent( step, {duration:800});
    this.remove(); // remove this from the DOM
  }

}
