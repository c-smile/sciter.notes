
import * as db from "../db/db.js";
import * as state from "state.js";

import {VirtualList} from "components/virtual-select.js";

function NoteView(note, isCurrent, isSelected) {

  let urlFrom = note.getMeta("urlFrom");

  let atts = { "book-color": note.book.color || 0 };

  if( note.hasActiveEvent ) atts.class = "active-event";

  let text = urlFrom ? <text>from:<span.www>{ (new URL(urlFrom)).hostname}</span></text>
                       : <text/>;

  let tags = note.tags?.filter( tag => tag.isCustom && tag.color )
                         .map( tag => <tag color={tag.color}>{tag.name}</tag>);

  return <option key={note.id}
      state-current={isCurrent}
      state-checked={isSelected} {atts}>
    <div><picture value={note.preview} /><caption>{note.caption}</caption>{text}</div>
    <tags>{tags}</tags>
  </option>;
}

class NotesList extends VirtualList {
  filter;
  notes = [];

  this(props) {
    super.this(props);
    this.renderItem = NoteView; 
    this.filter = props.filter;
    this.notes = Array.from(db.matchingItems(this.filter));
  }

  itemAt(at) {
      return this.notes[at];
  }

  totalItems() {
    return this.notes.length;
  }

  indexOf(item) {
    return this.notes.indexOf(item);
  }

  // overridable
  //renderItem(item, isCurrent, isSelected) {}

  renderList(items) {
    return <notes styleset={__DIR__ + "notesbar.css#noteslist"}>{ items }</notes>; 
  }

  currentDidChange() {
    db.signals.currentNote.value = this.currentItem;
    return true;
  }
 
}

class InputSearch extends Element {
  render() {
    return <input|text.search />;
  } 

  onchange() { 
    this.timer(400ms, this.submit);
  }

  onmousedown(evt) {
    if(evt.isOnIcon === this && !this.state.empty) {
      this.value = "";
      this.submit();
    }
  } 

  submit() {
    const text = this.value.toLowerCase().trim();
    if( text )
      db.signals.updateFilter({words:text.split(" ")});
    else 
      db.signals.updateFilter({words:null});
  }
}

function Book({book}) {
  function remove() {
    db.signals.updateFilter({book:null});
    return true;
  }
  return <span.filter.book onclick={remove}>{book.name}</span>;
}

function Tag({tag}) {
  function remove() {
    const tags = db.signals.filter.value.tags.filter(t => t !== tag);
    db.signals.updateFilter({tags: tags.length ? tags : null});
    return true;
  }
  return <span.filter.tag onclick={remove}>{tag.name}</span>;
}

function Tags({tags}) {
  return tags.map(tag => <Tag tag={tag} />);
}

class EventsButton extends Element {
  render() {
    const counter = db.signals.pendingEventsCounter.value;
    if(counter)
      return <button.calendar.pending @title="Calendar of events" checked={state.mainViewType.value === "events"}>
        <span.pending>{counter}</span>
      </button>;
    else 
      return <button.calendar @title="Calendar of events" checked={state.mainViewType.value === "events"} />;
  }
  onclick() {
    state.mainViewType.value = state.mainViewType.value === "events" ? "note" : "events";
    return true;
  }
}

function ToolBar({filter}) {
  return <toolbar.content>
    <InputSearch />
    <EventsButton />
  </toolbar>
}

function Filters({filter}) {
  if(!filter.book && !filter.tags)
    return [];
  return <toolbar.content.tags>
    { filter.book ? <Book book={filter.book}/> : [] }
    { filter.tags ? <Tags tags={filter.tags}/> : [] }
  </toolbar>
}

export class NotesBarView extends Element {
  constructor() {
    super();
    +db.signals.bookChanged; // subscribe to bookChanged signal 
    +db.signals.tagChanged;
    +db.signals.tagSetChanged;
    +db.signals.bookNotesChanged; 
    +db.signals.noteChanged;
  }
  render() {
    const filter = db.signals.filter.value;
    return <section.notesbar styleset={__DIR__ + "notesbar.css#notesbar"}>
      <ToolBar filter={filter} />
      <Filters filter={filter} />
      <NotesList filter={filter} />
    </section>   
  }

}