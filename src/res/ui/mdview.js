
//const SS = CSS.set`
//  :root { size:*; }
//`;

import {md2html,html2md} from './remark/md.js';
import * as state from './state.js';

const RE_HEADER = /^ *#{1,5} .*/;
const RE_CODEBLOCK = /^ *\`\`\` *$/;

function colorize(plaintext) {
  let codeblock = false;
  for(const eltext of plaintext.children) {
    const nodeText = eltext.firstChild;
    const text = nodeText.data;
    if( codeblock ) {
      eltext.classList.add("code");
      if( RE_CODEBLOCK.test(text) )
        codeblock = false; 
    } else if( RE_CODEBLOCK.test(text) ) {
        eltext.classList.add("code");
        codeblock = true; 
    } else {
      eltext.classList.remove("code");
      eltext.classList.toggle("header", RE_HEADER.test(text));
    }
  }
}

export class MDView extends Element {

  observers = []; // bound UI objects

  render() {

    const text = state.data.hasNonMDMarkup 
      ? <div.warning @>The note contains HTML constructs that cannot be represented by Markdown directly. 
           After saving (<icon|arrow-left />) your changes use Undo if you are not satisfied by the result, or just close (<icon|arrow-right />) without saving.</div>
      : <div.filler>Markdown</div>;

    return <section.md styleset={__DIR__ + "mdview.css#md-view"}>
      <toolbar.content>
        <button(save) @title="Save" />
        <splitter/>
        <button(undo) @title="Undo" />
        <button(redo) @title="Redo" />
        {text}
        <button(cancel) @title="Close" accesskey="!Escape" />
      </toolbar>
      <plaintext value={state.data.md} />
    </section>;
  }

  componentDidMount() {
    for(const todisable of document.$$(".disable-on-source"))
      todisable.state.disabled = true;
    const pt = this.$("plaintext");
    colorize(pt);
    this.setupToolbar();
    this.timer(20,()=>{ pt.state.focus = true; });
  }
  componentWillUnmount() {
    for(const todisable of document.$$(".disable-on-source"))
      todisable.state.disabled = false;
  }

  setupToolbar() {

    const plaintext = this.$("plaintext");

    const AVAILABLE = 0x00; // toolbar button flags
    const ACTIVE    = 0x01;
    const DISABLED  = 0x02;

    // binds UI thing (e.g. toolbar button) with command|function to be executed onClick
    let bind = (selector, cmd, param) => {
      var uiel = this.$(selector); //assert uiel;
      if(typeof cmd == "function") {
        uiel.on("click", () => cmd.call(plaintext,"exec",param,uiel) );
        this.observers.push( () => {
          var cmdState = (current ? cmd.call(plaintext,"query",param,uiel) : 0) || 0;
          uiel.state.checked  = (cmdState & ACTIVE) != 0;
          uiel.state.disabled = (cmdState & DISABLED) != 0;
        });
      } else if(typeof cmd == "string") {
        uiel.on("click", () => plaintext.execCommand(cmd,param) );
        this.observers.push( () => {
          var cmdState = (plaintext ? plaintext.queryCommand(cmd,param) : 0) || 0;
          uiel.state.checked  = (cmdState & ACTIVE) != 0;
          uiel.state.disabled = (cmdState & DISABLED) != 0;
        });
      } else 
        throw "Bad cmd type " + cmd;
    };

    bind("button(undo)", "edit:undo");
    bind("button(redo)", "edit:redo");

    this.updateObservers();
  }  

  updateObservers() { 
    for(let func of this.observers) func(); 
  }

  ["on statechange at plaintext"](evt) {
    this.timer(200,this.updateObservers); // NOTE: throttling updateObservers call
                                          //       to avoid frequent updates.
  }   

  get value() {
    return this.$("plaintext").value;
  } 

  close() {
    const ed = this.$("plaintext");
    if(ed.plaintext.isModified) {
      const r = Window.this.modal(
          <question>
            <content>Markdown was modified</content>
            <buttons>
               <button id="yes" role="default-button">Update note</button>
               <button id="no" role="cancel-button">Discard changes</button>
            </buttons>
          </question>);
      if(r === "yes")
        return this.save();
    }
    state.mainViewType.value = "note";
    return true;
  }

  async save() {
    let html = await md2html(this.value);
    state.data.htmlarea.bodyHTML = String(html);
    state.mainViewType.value = "note";
    return true;
  }

  ["on click at button(cancel)"]() { return this.close(); }
  ["on click at button(save)"]() { return this.save(); }

  ["on input at plaintext"](e,plaintext) {
    this.timer(40ms, function() { colorize(plaintext); });
  }

  static async show(htmlarea) {
    let html = htmlarea.bodyHTML;
    const file = await html2md(html);
    state.data.md = String(file);
    state.data.hasNonMDMarkup = htmlarea.hasNonMDMarkup;
    state.data.htmlarea = htmlarea;
    state.mainViewType.value = "md";
    return true;
  }

}