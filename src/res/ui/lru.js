
import * as db from "../db/db.js";

const { signal, effect } = Reactor;

export const LRU = signal([]);

effect( () => {
  const note = db.signals.currentNote.value;
  if(note) addNoteToLRU(note);
});

export function addNoteToLRU(note) {
  const index = LRU.value.indexOf(note); 
  if(index == 0) return;
  if(index > 0) LRU.value.splice(index,1);
  LRU.value = [note, ...LRU.value];
}

export function removeNoteFromLRU(note) {
  LRU.value = LRU.value.filter(n => n !== note);
}

export function clearLRU() {
  LRU.value = [];
}

