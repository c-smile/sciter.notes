
import * as env from "@env";
import {fs} from "@sys";
import {decode,encode,import as loadModule} from "@sciter";
import * as config from "../config.js"; 
import * as db from "../db/db.js";

import {openSettings,initSettings,saveState} from "boot/settings.js";

let InitialView;
let OperationalView;

function showWindow() {
  Window.this.state = Window.WINDOW_SHOWN;
}

export class App extends Element {

  operational = false; // 
  //settings = null;
  //wasSettingsSaveFailure = false;

  constructor() {
    super();

    const settings = openSettings(showWindow) ?? null;
    if(settings && settings.dbPath) {
      if(!db.openDatabase(settings.dbPath)) {
        Window.this.modal(<alert>Cannot open database at {dbPath}</alert>);
        return false;
      }
      this.operational = true;
    }
  }

  render() {
    let content;
    // NOTE: we are loading these two principal (a.k.a. "root") views dynamically not statically,
    //       just to avoid interference (and needless memory allocations) between boot and operational modes.
    if(this.operational) {
      content = OperationalView = OperationalView ?? loadModule(__DIR__ + "main.js").OperationalView;
    } else {
      content = InitialView = InitialView ?? loadModule(__DIR__ + "boot/view.js").InitialView;
    }
    return <body>{JSX(content,{},[])}</body>;
  }

  onGotDataPaths(settingsPath,dbPath) {
    if(!db.openDatabase(dbPath)) {
      Window.this.modal(<alert>Cannot open database at {dbPath}</alert>);
      return false;
    }

    initSettings(settingsPath,{dbPath});

    this.componentUpdate({operational: true});
    saveState();
    return true;
  }

}

