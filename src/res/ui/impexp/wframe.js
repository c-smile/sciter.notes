import {encode,decode,fromBase64,toBase64,md5} from "@sciter";
import {fs} from "@sys";

import {canonicalizeDOM} from "../editor/canonicalize.js";

const INLINE_CSS = 
`html{font:10pt Verdana,Helvetica,Roboto,sans-serif;}
table {border-collapse:collapse; }
table td, table th {border:1px solid #ddd; padding:4px 8px;}
pre{padding:4px;border:1px solid #ccc;background: #F0F8FF;max-width:100%;overflow-x:auto;}
code{background: #F0F8FF;}
dt{font-weight:bold;}
img{max-width:100%;}
blockquote {border-left:0.2em solid #84C5F4;margin-left:1em;padding-left:0.8em;}
p.urlfrom {text-align:right;border-top:1px solid;}
mark.a { background:#fcfc93;}  
mark.b { background:#a0f7a3;}  
mark.c { background:#e0a0f7;}`;

// used for note previews or for importing notes from files, applied to <frame>

export class WorkerFrame extends Element
{
  activators = {}; // activator classes map, to trace activators state 
  inlineCss;
  loading = false;
  note;

  render() {
    return <frame style="visibility:none" />;
  }

  // the document has issued data request
  onrequest(rq) {
    if( this.note )
      this.note.onrequest(rq);
  }

  get document() {
    return this.frame.document;
  }
  get body() {
    return this.document.body;
  }

  loadNote(n) {
    if( (this.note = n) && n.html) {
      this.loading = true;    
      this.frame.loadHtml(n.html, "note:" + n.id);
      this.loading = false;    
      return true;
    }
    return false;
  }

  static saveNoteToFile(n,pathname) {
    document.append(<WorkerFrame #worker/>);
    let worker = document.$("frame#worker");
    worker.saveNoteToFile(n,pathname);
    worker.remove();
  }

  static async saveNotesToFiles(entries,progress) {
    document.append(<WorkerFrame #worker/>);
    let worker = document.$("frame#worker");
    let n = 0;
    for(let [fn,node] of entries) {
      if(!await worker.saveNoteToFile(node,fn))
        throw new Error(`Error saving file ${fn}`);
      if(progress) progress(n++);
    }
    worker.remove();
  }


  // saves note to file with images inline
  async saveNoteToFile(n,pathname) 
  {
    if(!this.loadNote(n))
      return;
    
    for(const img of this.document.$$("picture[src],img[src]"))
    {
      const src = img.attributes["src"];
      const [data,mime] = this.note.getResource(src);
      const dataUrl = printf("data:%s;src=%s;base64,%s",mime, encodeURIComponent(src),toBase64(data));
      img.attributes["src"] = dataUrl;
    }

    const tags = this.note.tags.map( tag => tag.name ).join(",");

    const head = this.document.$("head");
    if( head ) head.remove();

    const cdate = this.note.cdate instanceof Date ? this.note.cdate : new Date();
    const mdate = this.note.mdate instanceof Date ? this.note.mdate : new Date();

    this.document.prepend(<head>
      <title>{this.note.caption}</title>
      <meta name="note-id" content={this.note.id} />
      <meta name="note-cdate" content={cdate.toISOString()} />
      <meta name="note-mdate" content={mdate.toISOString()} />
      <meta name="note-created-by" content={this.note.createdBy} />
      <meta name="note-modified-by" content={this.note.modifiedBy} />
      <meta name="note-book" content={this.note.book.name} />
      <meta name="note-tags" content={tags} />
      <style>{INLINE_CSS}</style>
    </head>);

    try {
      const data = this.frame.saveBytes();
      if(data) {
        const file = await fs.open(pathname,"w");
        await file.write(data);
        await file.close();
      }
      //this.frame.saveFile(pathname);
      return true;
    } catch (e) {
      console.error(e,e.stack);
      return false;
    }
  }

/*
  // loads note from file and stores it in DB
  // function conflictResolver(existingNote, importingNote) returns:
  //   #update - but keep changes history
  //   #discard - discard changes as we have more recent changes
  //   #merge - TODO
  createNoteFromFile(pathOrBytes, params = {}) 
  {
    let note = null;
    let doc = null;
    let generator = null;

    function crackDataUrl(durl) {
      let [head,data] = durl.split(",");

      data = fromBase64(data);

      console.assert(data instanceof ArrayBuffer,"wrong data URL");
     
      let heads = head.split(";",2);
      let mimetype;
      let src = "cid:" + data.md5(); // default src 

      console.assert(data instanceof ArrayBuffer,"wrong data URL");
      if( heads.last != "base64" )
         return durl; // not a base64 ?
      if(heads.length == 2) 
        mimetype = heads[0]; 
      else if(heads.length >= 3) {
        mimetype = heads[0];
        const [srcn,srcv] = split(heads[1],"=");
        if(srcn === "src")
          src = srcv.urlUnescape();
      } else
        return durl;

      note.addResource(src, mimetype, data);
      Tag.known("image",true).addItem(note);
      return src;
    }

    function processDataUrls(doc) // make relative URLS
    {
      for( let el of doc.$$('[src^="data:"]') ) 
        el.attributes["src"] = crackDataUrl(el.attributes["src"]);
    }

    let noteId;
    let noteCaption;
    let noteCDate;
    let noteMDate;
    let noteBook; // name
    let noteTags; // comma separated list
    let noteCWho; 
    let noteMWho; 

    function processNote(evt)
    {
      note = new Note();
      doc = evt.target;

      noteId = params.id || DOM.getMeta(doc,"note-id") || note.id;
      noteCDate = params.cdate || Date.parse(DOM.getMeta(doc,"note-cdate"), note.cdate);
      noteMDate = params.mdate || Date.parse(DOM.getMeta(doc,"note-mdate"), note.mdate); 
      noteCWho =  DOM.getMeta(doc,"note-created-by") || note.createdBy;
      noteMWho =  DOM.getMeta(doc,"note-modified-by") || note.modifiedBy;

      noteCaption = DOM.getTitle(doc);

      noteBook = DOM.getMeta(doc,"note-book") || db.defaultBook().name;
      noteTags = DOM.getMeta(doc,"note-tags"); 

      generator = canonicalizeDOM(doc,{});
      processDataUrls(doc);
    }

    this.on("parsed",processNote);

    let action;

    if((typeof pathOrBytes == "string" ? this.frame.loadFile(pathOrBytes) : this.frame.loadHtml(pathOrBytes)) && note) 
    {
      this.note = note;
      note.id = noteId; 
      note.cdate = noteCDate;
      note.mdate = noteMDate; 
      note.createdBy = noteCWho;
      note.modifiedBy = noteMWho;

      let text = this.body.innerText;
      let html = this.frame.saveBytes(); // to ArrayBuffer

      let caption = DOM.caption(doc);

      note.html = html;
      note.text = text;
      note.caption = caption;

      let book = params.book || db.getBookByName(noteBook,true);
      let tags = noteTags.split(",").filter( nt => !!nt).map( tn => db.getTagByName(tn,true));

      ([note,action] = db.importItem(note,book,tags));

      // TODO: reload if updated to get proper preview
      let previewImage = previewImageOf(doc);
      note.preview = previewImage.toBytes();
      //previewImage.destroy();
    }
    this.off(processNote);
    return [note,action];
  }
  */
}
