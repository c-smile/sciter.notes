
import * as db from "../../db/db.js";
import {fs} from "@sys";
import {sanitize} from "./sanitize-fn.js";
import {WorkerFrame} from "./wframe.js";
import {Progress} from "../progress.js";

export async function exportNotes(notes,folder,cbProcessed) {
  const map = {};
  for(const note of notes) {
    let fn = folder + sanitize(note.caption," ").trim() + ".htm";
    if(map[fn]) fn = folder + note.id + ".htm";
    map[fn] = note;
  }
  const entries = Object.entries(map);  
  try {
    await WorkerFrame.saveNotesToFiles(entries,cbProcessed);
    return true;
  } catch(e) {
    Window.this.modal(<alert>{e} {e.stack}</alert>);
  }
}

export async function exportBookNotesTo(book,bookfolder) {

  document.append(<Progress #progress caption=`Exporting ${book.name} notes` />);
  const prog = document.$("#progress");

  let total = book.items.length;
  let processed = 0;
  function noteProcessed(n) { prog.progress = ++processed / total; }
  try {
    if(!fs.sync.stat(bookfolder)) 
      await fs.mkdir(bookfolder);
    await exportNotes(book.items, bookfolder + "/", noteProcessed);
  } catch(e) {
    Window.this.modal(<alert>{e} {e.stack}</alert>);
  }
  prog.shutdown();
}

export async function exportAllNotesTo(folder) {

  document.append(<Progress #progress caption="Exporting all notes" />);
  const prog = document.$("#progress");

  const books = db.getBooks(db.getDeletedBook());

  let total = db.totalItems();
  let processed = 0;
  function noteProcessed(n) {
    prog.progress = ++processed / total;
  }

  try {
    for(const book of books) {
      const bookfolder = folder + sanitize(book.name);
      if(!fs.sync.stat(bookfolder)) 
        await fs.mkdir(bookfolder);
      await exportNotes(book.items, bookfolder + "/", noteProcessed);
    }
  } catch(e) {
    Window.this.modal(<alert>{e} {e.stack}</alert>);
  }

  prog.shutdown();
}