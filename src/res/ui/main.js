
import * as env from "@env";
import {fs} from "@sys";
import {decode,encode} from "@sciter";
import * as config from "../config.js"; 
import * as db from "../db/db.js";
import * as Settings from "./boot/settings.js";
import "./boot/check-version.js";

import {SideBarView} from "./sidebar.js";
import {NotesBarView} from "./notesbar.js";
import {NoteView} from "./noteview.js";
import {EventsView} from "./eventsview.js";
import {MDView} from "./mdview.js";
import {SrcView} from "./srcview.js";
import {LRUView} from "./lruview.js";

import * as state from "./state.js";

const {signal,effect} = Reactor;

//const pathname = env.path("documents", config.DBNAME);
//const pathname = "E:/" + config.DBNAME;
//if( !db.openDatabase(pathname) )
//  console.log("FAIL", pathname);

class MainView extends Element {

  constructor() {
    super();
    // when we have current note changed anyhow, hide calendar if it is shown.
    this.currentNoteChanged = effect(() => {
      if(db.signals.currentNote.value)
       state.mainViewType.value = "note";
    });
  }

  render() {
    return <section.mainview view-type={state.mainViewType.value}>
      <NoteView />
      { (state.mainViewType.value == "events") && <EventsView /> }
      { (state.mainViewType.value == "md") && <MDView /> }
      { (state.mainViewType.value == "src") && <SrcView /> }
    </section>;
  }
}


export class OperationalView extends Element {

  constructor() {
    super();
    Settings.add(this);
  }

  render() {
    return <frameset cols>
          <SideBarView.disable-on-source />
          <splitter/>
          <NotesBarView.disable-on-source />
          <splitter/>
          <MainView />
          <splitter/>
          <LRUView.disable-on-source/>
        </frameset>;
  }

  // persistable

  store(data) {
    data["frameset"] = this.frameset.state.map( w => w.toString() );
  }

  restore(data) {
    const fss = data["frameset"];
    if(fss) this.frameset.state = fss;
  }

  onstatechange() {
    Settings.saveState(); 
  }

}

