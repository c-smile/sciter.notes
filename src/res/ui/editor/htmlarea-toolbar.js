//import {encode,decode} from "@sciter";

import {COMMAND_SYM} from "../../config.js";

import {EventWidget} from "./content/widgets.js";

export class HTMLAreaToolbar extends Element {
  note;
  htmlarea;

  this({note}) {
    if(note !== this.note) {
      this.note = note;
      this.post( () => this.doUpdate() );
    }
  }

  componentDidMount() {
    this.htmlarea = this.$p("#note-view").htmlarea;
    const requestUpdate = e => { this.timer(500ms, this.doUpdate) };  
    this.htmlarea.on("change", requestUpdate);
    this.htmlarea.on("statechange", requestUpdate);
  }

  doUpdate() { this.patch(this.render()); }

  render() {
    const htmlarea = this.htmlarea;
    return <toolbar.content.band styleset={__DIR__ + "htmlarea.css#toolbar"}>
      <Command htmlarea={htmlarea} command="edit:undo" @title="undo" />
      <Command htmlarea={htmlarea} command="edit:redo" @title="redo" />

      <div>
        <SpanCommand(span) htmlarea={htmlarea} @title="spans" />
        <BlockCommand(block) htmlarea={htmlarea} @title="blocks" />
        <WidgetCommand(widget) htmlarea={htmlarea} @title="widgets" />

        <Command.list-ul  htmlarea={htmlarea} command="format:toggle-list:ul"  @title="Unordered list" />
        <Command.list-ol  htmlarea={htmlarea} command="format:toggle-list:ol"  @title="Ordered list" />
        <Command.list-dir htmlarea={htmlarea} command="format:toggle-list:dir" @title="Check list" />
        <Command.list-dl  htmlarea={htmlarea} command="format:toggle-list:dl"  @title="Definition list" />
        <Command.unindent htmlarea={htmlarea} command="format:unindent"  @title="Unindent" />
        <Command.indent   htmlarea={htmlarea} command="format:indent"  @title="Indent" />
        <Command.blockquote htmlarea={htmlarea} command="format:toggle-block:blockquote"  @title="Quote block" />
        <Command.pre htmlarea={htmlarea} command="format:toggle-pre"  @title="Code block" />
        <MenuButton.table htmlarea={htmlarea} @title="Table"><TableSelector htmlarea={htmlarea} /></MenuButton>
        <ImageCommand.image htmlarea={htmlarea} @title="Image" />
        <LinkCommand htmlarea={htmlarea} />
      </div>
    </toolbar>;
  }

}

const CMD_AVAILABLE = 0;
const CMD_SELECTED  = 0x01;
const CMD_DISABLED  = 0x02;

function Command({command,htmlarea,title}) {
  const status = htmlarea?.queryCommand(command);
  //console.log(command,status);
  function onclick() { htmlarea.execCommand(command); htmlarea.state.focus = true; return true; }
  return <button name={command} 
            onclick={onclick} 
            state-disabled={(status & CMD_DISABLED) != 0} 
            state-checked={(status & CMD_SELECTED) != 0} 
            title={title} />;
}

function ImageCommand({htmlarea,title}) {
  const status = htmlarea?.queryCommand("edit:insert-html");  
  function onclick() { 
    const filter = "Image files|*.png;*.jpg;*.jpeg;*.svg;*.webp|All Files|*.*";
    const fn = Window.this.selectFile({ filter, mode:"open" });
    if(fn) 
      htmlarea.insertHTML(`<img src="${fn}" />`);

    htmlarea.state.focus = true; 
    return true; 
  }
  return <button onclick={onclick} 
            state-disabled={(status & CMD_DISABLED) != 0} 
            state-checked={(status & CMD_SELECTED) != 0} 
            title={title} />;
}


function LinkCommand({htmlarea}) {
  const status = htmlarea?.queryCommand("format:remove-span:a");  

  function onclick() {
    this.post(() => { LinkEditor.show(htmlarea) });
    return true;
  }

  if(status == CMD_DISABLED) {
    // no link under cursor/selection
    return <button.link onclick={onclick} 
              state-disabled={htmlarea.selection.isCollapsed} 
              state-checked={false} 
              @title="Add link" />;
  } else {
    // link is under cursor/selection
    return <button.unlink onclick={onclick} 
              state-disabled={false} 
              state-checked={true} 
              @title="Change ot remove link" />;
  }
}

function SpanCommand({command,htmlarea,title}) {

    function MenuItem({text,accesskey,command,param}) {
      const status = htmlarea?.queryCommand(command); 
      function onclick() { htmlarea?.execCommand(command,param); return true; }
      return <li 
         state-disabled={(status & CMD_DISABLED) != 0} 
         state-checked={(status & CMD_SELECTED) != 0} 
         onclick={onclick}><label>{text}</label><span class="accesskey">{accesskey}</span></li>;
    }

    function popupRequest(evt) {
      evt.source = Element.create(
        <menu.popup>
          <MenuItem class="remove"     @text="Remove formatting" accesskey="ALT+Delete" command="format:remove-span:*" />
          <hr/>
          <MenuItem class="strong"     @text="Strong emphasis" accesskey={COMMAND_SYM +"B"} command="format:toggle-span:b|strong" />   
          <MenuItem class="em"         @text="Emphasis" accesskey={COMMAND_SYM +"I"} command="format:toggle-span:i|em" />
          <MenuItem class="underline"  @text="Underline" accesskey={COMMAND_SYM +"U"} command="format:toggle-span:u" />
          <MenuItem class="del"        @text="Deletion" accesskey={COMMAND_SYM +"E"} command="format:toggle-span:del|s|strike" />
          <MenuItem class="sub"        @text="Subscript" accesskey={COMMAND_SYM +"S+B"} command="format:toggle-span:sub" />
          <MenuItem class="sup"        @text="Superscript" accesskey={COMMAND_SYM +"S+P"} command="format:toggle-span:sup" />
          <MenuItem class="code"       @text="Inline code" accesskey={COMMAND_SYM +"D"} command="format:toggle-span:code" />
          <hr/>
          <MenuItem class="mark a"     @text="Marking A" accesskey={COMMAND_SYM +"M+A"} command="format:apply-span:mark" param={{"class":"a"}} />
          <MenuItem class="mark b"     @text="Marking B" accesskey={COMMAND_SYM +"M+B"} command="format:apply-span:mark" param={{"class":"b"}} />
          <MenuItem class="mark c"     @text="Marking B" accesskey={COMMAND_SYM +"M+C"} command="format:apply-span:mark" param={{"class":"c"}} />
        </menu>);
      return true;
    }
    return <button|menu onpopuprequest={popupRequest} title={title} />;
}

function BlockCommand({command,htmlarea,title}) {

    function MenuItem({text,accesskey,command,param}) {
      const status = htmlarea?.queryCommand(command); 
      function onclick() { htmlarea?.execCommand(command,param); return true; }
      return <li 
         state-disabled={(status & CMD_DISABLED) != 0} 
         state-checked={(status & CMD_SELECTED) != 0} 
         onclick={onclick}><label>{text}</label><span class="accesskey">{accesskey}</span></li>;
    }

    function popupRequest(evt) {
      evt.source = Element.create(
        <menu.popup>
          <MenuItem class="p"   @text="Paragraph" accesskey={COMMAND_SYM +"NUMPAD0"} command="format:morph-block:p" />
          <MenuItem class="div" @text="Division"  accesskey={COMMAND_SYM +"NUMPAD7"} command="format:morph-block:div" />
          <MenuItem class="h1"  @text="Heading 1" accesskey={COMMAND_SYM +"NUMPAD1"} command="format:morph-block:h1" />
          <MenuItem class="h2"  @text="Heading 2" accesskey={COMMAND_SYM +"NUMPAD2"} command="format:morph-block:h2" />
          <MenuItem class="h3"  @text="Heading 3" accesskey={COMMAND_SYM +"NUMPAD3"} command="format:morph-block:h3" />
          <MenuItem class="h4"  @text="Heading 4" accesskey={COMMAND_SYM +"NUMPAD4"} command="format:morph-block:h4" />
          <MenuItem class="h5"  @text="Heading 5" accesskey={COMMAND_SYM +"NUMPAD5"} command="format:morph-block:h5" />
          <MenuItem class="h6"  @text="Heading 6" accesskey={COMMAND_SYM +"NUMPAD6"} command="format:morph-block:h6" />
        </menu>);
      return true;
    }
    return <button|menu onpopuprequest={popupRequest} title={title} />;
}

function WidgetCommand({command,htmlarea,title}) {

    function MenuItem({text,accesskey,command,param}) {
      const status = htmlarea?.queryCommand(command); 
      function onclick() { htmlarea?.execCommand(command,param); return true; }
      return <li 
         state-disabled={(status & CMD_DISABLED) != 0} 
         state-checked={(status & CMD_SELECTED) != 0} 
         onclick={onclick}><label>{text}</label><span class="accesskey">{accesskey}</span></li>;
    }

    const createEvent = () => { EventWidget.create(htmlarea); return true; }

    function popupRequest(evt) {
      evt.source = Element.create(
        <menu.popup>
          <li.event onclick={createEvent} @>New event</li>
        </menu>);
      return true;
    }
    return <button|menu onpopuprequest={popupRequest} title={title} />;
}

function MenuButton({htmlarea,title},kids) {
    return <button|menu title={title}>{kids}</button>;
}


const tableSelectorStyles = CSS.set`
   :root { background:var(popup-back); border: 1px solid var(widget-border); }   
   table {border-spacing:1px; }
   th,td { size:2em; }
   th.cols { background:transparent no-repeat 50% 50% url(icon:radio-off); }
   th.cols.on { background-image:url(icon:radio-on); }
   th.rows { background:transparent no-repeat 50% 50% url(icon:radio-off); }
   th.rows.on { background-image:url(icon:radio-on); }
   td { border:1px solid; }
   td.header {background-color:#ddd; }   
   td.on { foreground-color:color(highlight-hover); }
   td:hover { outline:1px solid color(accent); }
`;

class TableSelector extends Element {

  htmlarea;

  headerRows = 1;
  headerCols = 0;

  cols = 0;
  rows = 0;

  this({htmlarea}) {
    this.htmlarea = htmlarea;
  }

  render() {

    const { headerRows, headerCols, cols, rows, htmlarea } = this;

    function THc(c) { 
      //if(c == 0) return headerRows == 0 ? <th.cols.on /> : <th.cols />; 
      return c == headerCols ? <th.cols.on /> : <th.cols />; 
    }
    function THr(r) { 
      if(r == 0) return headerCols == 0 ? <th.cols.on /> : <th.cols />; 
      return r == headerRows ? <th.rows.on /> : <th.rows />; }
    function TD(r,c) { 
      //const atts =  ? {header:true}:{};
      let cls = r <= rows && c <= cols ? "on" : "";
      if( r <= headerRows || c <= headerCols ) cls += " header";
      return <td class={cls} />; 
    }

    const tbody = [];

    for(let r = 0; r < 9; ++r) {
      const row = []
      if(r == 0) for(let c = 0; c < 9; ++c) { if(c == 0) row.push(THr(r)); else row.push(THc(c)); }
      else for(let c = 0; c < 9; ++c) { if(c == 0) row.push(THr(r)); else row.push(TD(r,c)); }
      tbody.push(<tr>{row}</tr>);
    }

    return <menu.popup styleset={tableSelectorStyles}>
      <table><tbody>{tbody}</tbody></table>
    </menu>;
  }

  ["on mousemove at td"](e,td) {
    const cols = td.elementIndex;
    const rows = td.parentElement.elementIndex;
    this.componentUpdate({cols,rows});
  }
  ["on mouseup at td"](e,td) {
    //const cols = td.elementIndex - 1;
    //const rows = td.parentElement.elementIndex - 1;
    this.doInsertTable();
    this.state.popup = false;
  }
  ["on mousedown at th.cols"](e,th) {
    const headerCols = th.elementIndex;
    if(headerCols)
      this.componentUpdate({headerCols});
    else
      this.componentUpdate({headerCols:0,headerRows:0});
  }
  ["on mousedown at th.rows"](e,th) {
    const headerRows = th.parentElement.elementIndex;
    if(headerRows)
      this.componentUpdate({headerRows});
    else
      this.componentUpdate({headerCols:0,headerRows:0});
  }
  ["on mouseout"](e,td) {
    this.componentUpdate({cols:0,rows:0});
  }

  doInsertTable() {

    let { headerRows, headerCols, cols, rows, htmlarea } = this;

    let table = "<table>";

    if( headerRows > rows ) headerRows = rows;
    if( headerCols > cols ) headerCols = cols;

    if(headerRows > 0) {
      table += "<thead>";
      for(let r = 0; r < headerRows; ++r) {
        table += "<tr>";
        for(let c = 0; c < cols; ++c)
          table += "<th/>";
        table += "</tr>";
      }
      table += "</thead>";
    }

    rows -= headerRows; 

    if(rows > 0) {
      table += "<tbody>";
      for(let r = 0; r < rows; ++r) {
        table += "<tr>";
        for(let c = 0; c < cols; ++c) {
          if( c < headerCols )
            table += "<th/>";
          else 
            table += "<td/>";
        }
        table += "</tr>";
      }
      table += "</tbody>";
    }
    table += "</table>";
    htmlarea.insertHTML(table); 
  }
}

const URL_RE = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+(:[0-9]+)?|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/;

class LinkEditor extends Element {
  htmlarea;
  target;
  constructor({htmlarea}) {
    super();
    this.htmlarea = htmlarea;    
    this.target = htmlarea.selection.$("a[href]"); 
  }

  componentDidMount() {
    this.$("input").state.focus = true;
  }

  render() {

    const val = this.target?.attributes["href"];

    const valid = !val || URL_RE.test(val);

    return <popup.link-props styleset={__DIR__ + "htmlarea.css#link-editor"}>
      <icon|link/> <input|text(url) value={val} class={valid && "valid"} />
      <button(apply).primary disabled/>
      { this.target ? <button(remove).danger /> : [] }
    </popup>;
  }

  ["on change at input"](e,input) {
    const valid = URL_RE.test(input.value);
    input.classList.toggle("valid",valid);
    this.$("button(apply)").state.disabled = !valid;
  }

  ["on ^keyup"](evt) {
    switch(evt.code) {
      case "Enter": this.apply(); this.state.popup = false; return true;
      case "Escape": this.state.popup = false; return true;
    }
  }

  apply() {
    const url = this.$("input(url)").value.trim();
    if( this.target ) {
      this.htmlarea.richtext.update(tctx => {
        this.target.setAttribute(this.target,"href",url);
        return true;
      });    
    } else {
      this.htmlarea.execCommand("format:apply-span:a", { href: url });
    }
    this.htmlarea.post(new Event("change",{bubbles:true}));
    return true;
  }

  ["on ^click at button(remove)"](evt) {
    this.htmlarea.execCommand("format:remove-span:a");    
    this.htmlarea.post(new Event("change",{bubbles:true}));
    this.state.popup = false;
    return true;
  }

  ["on ^click at button(apply)"](e,button) {
    this.apply();
    this.state.popup = false;
    return true;
  }

  static show(htmlarea) {
    let selectionType = htmlarea.selection.type;
    let srect = htmlarea.box("selection","window");
        srect >>= 4;
    const spos = srect.pointOf(2);
    htmlarea.ownerDocument.popup(<LinkEditor htmlarea={htmlarea}/>, { popupAt:8, x: spos.x, y: spos.y });
  }

}
