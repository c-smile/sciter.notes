
import * as db from "../../db/db.js";
import {HTMLArea} from "htmlarea.js";
import {HTMLAreaToolbar} from "htmlarea-toolbar.js";

const {signal,effect} = Reactor;

export class View extends Element {

  note;
  #htmlarea;
  #toolbarDock;

  this({note}) {
    if(this.note !== note) {
      if(!note) {
        note = !this.note?.isNew ? new db.Note() : this.note;
        document.post(() => {this.activate()});
      } 
      this.note = note;
      this.post(this.updateState);
    }
  }

  render() {
    const note = this.note;
    return <section #note-view styleset={__DIR__ + "view.css#editor"}>
      <NoteToolbarDock(dock) note={note} />
      <HTMLArea(editor) note={note} />
    </section>;
   }
  
  ["on actualize-note"]() {
    if(this.note?.isNew) {
      //console.log("actualize-note", this.note.book);
      db.addItem(this.note);
      db.signals.currentNote.value = this.note;
    }
    return this.note;
  }

  get htmlarea() { 
    return this.#htmlarea ?? (this.#htmlarea = this.$("htmlarea"));
  }
  get toolbarDock() { 
    return this.#toolbarDock ?? (this.#toolbarDock = this.$("toolbar(dock)"));
  }

  activate() {
    this.htmlarea.state.focus = true;
  }

  updateState() {
    this.setLock(this.note.lock);
  }

  ["on click at button(lock)"](e,button) {
     this.setLock(!this.note.lock);
  }

  setLock(onOff) {
    //const block = this.$("button(lock)");
    this.note.lock = onOff;
    this.$("button(lock)").state.checked = !onOff;
    this.htmlarea.state.readonly = onOff;
    this.toolbarDock.componentUpdate();
  }


}
 
function NoteInfoToolbar({note}) {

  const sameDay = (first, second) =>
    first.getFullYear() === second.getFullYear() &&
    first.getMonth() === second.getMonth() &&
    first.getDate() === second.getDate();

  if(sameDay(note.cdate,note.mdate) && (note.modifiedBy === note.createdBy))
    return <toolbar.content.band.note-info>
      <span.name.created>{note.createdBy}</span>
      <span.date>{note.cdate.toLocaleDateString()}</span>
    </toolbar>;
  else 
    return <toolbar.content.band.note-info>
      <span.name.created>{note.createdBy}</span>
      <span.date>{note.cdate.toLocaleDateString()}</span>
      <span.name.modified>{note.modifiedBy}</span>
      <span.date>{note.mdate.toLocaleDateString()}</span>
    </toolbar>;
}


function DockContent({editing,note}) {
  return <section>
    { editing ? <HTMLAreaToolbar(editing) note={note} /> 
              : <NoteInfoToolbar(viewing) note={note} /> }
  </section>;
}

class NoteToolbarDock extends Element {
  note;

  this({note}) {
    this.note = note;
  }

  render() {
    const editing = !this.note?.lock;
    return <toolbar.content.dock>
      <DockContent editing={editing} note={this.note} />
      <button(lock) checked={editing}  />        
    </toolbar>;
  }

/*  switchView() {
    const params = { effect:"scroll-top", ease:"sine-in-out", duration:200 };
    //button.state.disabled = true;
    await content.replaceContent(nc, params);
    //button.state.disabled = false;
  }*/
}