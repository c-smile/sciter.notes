import {encode,decode} from "@sciter";
import {launch} from "@env";

import {canonicalizeDOM} from "./canonicalize.js";
import {Tag,Note,Book,signals,needsNoteUpgrade} from "../../db/db.js";
//import {HTMLAreaFloatingToolbar} from "./htmlarea-toolbar.js";

const emptyHTML = encode("<html><body><p></p></body></html>");

const PREVIEW_WIDTH = 80;
const PREVIEW_HEIGHT = 80;

function previewImageOf(body) {
  //const [px1,py1,px2,py2] = body.state.box("rect","border","inner");
  const [wmin,wmax] = body.state.contentWidths();
  const w = Math.min(wmax, 800);
  const h = Math.max(Math.min(body.state.contentHeight(w), 1000),32);
  const pvh = Math.min(PREVIEW_HEIGHT, PREVIEW_WIDTH * h / w );
  return new Graphics.Image(w, h, PREVIEW_WIDTH, pvh, body); // make snapshot of the body*/
}

export class HTMLArea extends Element {
  note;
  changes = false;
  highlightedWords;

  constructor() {
    super();
    this.highlightWordsEffect = Reactor.effect(() => { 
      const words = signals.filter?.value.words;
      if(words !== this.highlightedWords) {
        this.highlightWords(words);
      }
    });
  }

  this({note}) {
    if(note !== this.note) {
      this.saveNote();
      this.note = note;
      this.post( () => this.show(note) );
    }
  }

  componentDidMount() {
    this.classList.add("initial");
  }

  get bodyHTML() { return this.richtext.body.innerHTML; }
  set bodyHTML(v) { 
    // note: undoable transaction
    this.richtext.update(tctx => {
      tctx.setInnerHTML(this.richtext.body,v);
      return true;
    });
    this.onchange();
  }  

  get contentDocument() {
    return this.richtext.document;
  }
  get contentBody() {
    return this.richtext.body;
  }

  get hasNonMDMarkup() {
    const nonMDable = this.contentBody.$("dir,[colspan],[rowspan],td>br,th>br");
    return !!nonMDable;
  }

  onrequest(rq) { 
    if( this.note ) 
      this.note.onrequest(rq);
  } 

  onrequestresponse(rs) {
    this.ensureNote().onrequestresponse(rs);
  }

  onchange() {
    this.onchange_operational();
    this.classList.remove("initial"); // remove that initial placeholder    
    this.onchange = this.onchange_operational;
  }
  onchange_operational() {
    //console.log("onchange",this.changes);
    this.changes = true;
    this.timer(1s, this.saveNote);
  }

  onpastehtml (evt) {
    const docRoot = evt.source; // DOM element that usually contains: <html>,<body>,
    const fromUrl = evt.data.url; // behavior:richtext passes {url:...} object with meta data 
    const [generatorFromDom, title] = canonicalizeDOM(docRoot);
    var generator = evt.data.generator || generatorFromDom;

    if( this.note.isNew )
      this.post( () => {
        this.postProcessPasting({ sourceUrl: fromUrl, sourceGenerator : generator,sourceTitle : title });
      });
    this.onchange();
    // do not consume the event so it will be pasted
  } 

  ["on ^navigate"](evt) {  
    const {url} = evt.data;
    if(!url.startsWith("note:")) // TODODODODO
      launch(url);
    return true; 
  }

  ["on ^contextmenu at :root"](evt) {
    evt.source = Element.create(generateContextMenu(this));
    return true;
  }

  ensureNote() {
    if( this.note.isNew ) {
      this.dispatchEvent(new Event("actualize-note", {bubbles:true}));
      this.richtext.url = this.note.url;
      //this.postProcessPasting();
    }
    return this.note;
  }

  saveNote(force = false) {
    //console.log("saveNote",force,this.changes);
    if( !force && !this.changes )
        return;

    this.changes = false;

    this.ensureNote();

    const contentBody = this.richtext.body;

    const text = contentBody.innerText;
    const html = this.richtext.contentToBytes(); // ArrayBuffer
    
    const caption = contentBody.$("h1,h2,h3,p")?.innerText ?? contentBody.innerText;

    const previewImage = previewImageOf(contentBody);

    this.updateNoteEvents();
    
    this.note.update { 
      html,
      text, 
      caption, 
      preview: previewImage.toBytes("png")
    };
    
  }  

  render() {
    return <htmlarea content-style={__DIR__ + "content/content.css"} />;
  }

  show(note) {
    this.richtext.load(note?.html || emptyHTML, note?.url || "about:blank");
    if(this.highlightedWords)
      this.highlightWords(this.highlightedWords);
    if(needsNoteUpgrade())
      this.upgrade();
  }

  insertHTML(html) {
    this.execCommand("edit:insert-html",html);
    this.onchange();
  }

  upgrade() {
    const body = this.richtext.body;
    if(!body) return;
    let counter = 0;
    for(const ee of body.$$("input[type='event']")) {
      const dateISO = ee.attributes["value"];
      const warn = ee.attributes["warn-for"];
      const date = new Date(dateISO);//
      let alarm = "";
      switch(warn) {
         case "720" : alarm = "1m"; break;
         case "168" : alarm = "1w"; break;
         case "24" :  alarm = "1d"; break;
      }
      let href = `event:${dateISO}`;
      if(alarm) href += `?warn=${alarm}`
      ee.replaceWith(Element.create(<a href={href}>{date.toLocaleDateString()}</a>));
      ++counter;
    }
    if(counter) {

      this.saveNote(true);
    }
  }

  updateNoteEvents() {
    const body = this.richtext.body;
    if(!body) return null;
    const events = [];
    for(const ee of body.$$("a[href^='event:']"))
      events.push({ end: ee.endDate, start: ee.startDate });
    events.sort((a,b) => a.start.valueOf() - b.start.valueOf());
    this.ensureNote().setEvents(events);
  }

  ["on event-set-change"]() {
    this.post(this.updateNoteEvents);
    return true;
  }

  highlightWords(words) {
    const doc = this.contentDocument;
    const range = doc.createRange(this.contentBody);    
    if(this.highlightedWords?.length)
      range.clearMark();
    this.highlightedWords = words;
    if(words?.length)
      for(let node of range.nodes()) {
        if( !node.nodeIsText ) continue;
        let text = node.data.toLowerCase();
        doc.createRange(node).clearMark();
        for(let index = 0; index < words.length; ++index) {
          const word = words[index];
          let pos = text.indexOf(word);
          if( pos < 0 ) continue;
          doc.createRange(node, pos, node, pos + word.length).applyMark("found" + index);
        }
      } 
  }


  postProcessPasting({sourceGenerator,sourceUrl,sourceTitle}) {

    if(sourceGenerator) {
      if(sourceGenerator.includes("Word"))  { Tag.known("word",true).addItem(this.note); return; }
      if(sourceGenerator.includes("Excel")) { Tag.known("excel",true).addItem(this.note); return; }
    }

    // no luck with the generator, try source url
    if(sourceUrl) {
      const url = new URL(sourceUrl);
      switch(url.protocol) {
          case "http:":
          case "https:": { 
             Tag.known("web",true).addItem(this.note); 
             const domain = url.hostname;
             this.richtext.body.append(<p|urlfrom><a href={sourceUrl}>{domain}</a></p>);
             this.note.setMeta{ urlFrom: sourceUrl };
             break;
           }
          case "file:": {
            switch(url.extension) {
              case "doc":
              case "docx": Tag.known("word",true).addItem(this.note); break;
              case "xls":
              case "xlsx": Tag.known("excel",true).addItem(this.note); break;
            }
            break;
          }
      } 
    }

    if(sourceTitle) {
       // if note text does not contain h1,h2 then we are generating h1 from <head><title>
      if(!this.richtext.body.$("h1,h2"))
        this.richtext.body.prepend(<h1>{this.sourceTitle}</h1>);
    }
  }
} 

function generateContextMenu(htmlarea) {
   const basicItems = [
    <li command="edit:undo"><label>Undo</label><span class="accesskey">&platform-cmd-mod;+Z</span></li>,
    <li command="edit:redo"><label>Redo</label><span class="accesskey">&platform-cmd-mod;+&platform-shift-mod;+Z</span></li>,
    <hr/>,
    <li command="edit:cut"><label>Cut</label><span class="accesskey">&platform-cmd-mod;+X</span></li>,
    <li command="edit:copy"><label>Copy</label><span class="accesskey">&platform-cmd-mod;+C</span></li>,
    <li command="edit:paste"><label>Paste</label><span class="accesskey">&platform-cmd-mod;+V</span></li>,
    <hr/>,
    <li command="edit:selectall"><label>Select All</label><span class="accesskey">&platform-cmd-mod;+A</span></li>
  ];
    
  let tableItems = [];
  if(htmlarea.selection.isInTableCell || htmlarea.selection.isTableCellRange) {
    tableItems = [
       <li.break-before command="edit:insert-table-row:before"><label>Insert Row Before</label></li>,
       <li command="edit:insert-table-row:after"><label>Insert Row After</label></li>,
       <hr/>,
       <li command="edit:insert-table-column:before"><label>Insert Column Before</label></li>,
       <li command="edit:insert-table-column:after"><label>Insert Column After</label></li>,
       <hr/>,
       <li command="edit:merge-table-cells"><label>Merge cells</label></li>,
       <li command="edit:split-table-cells"><label>Split cells</label></li>,
       <hr/>,
       <li command="edit:delete-table-rows"><label>Delete Row(s)</label></li>,
       <li command="edit:delete-table-columns"><label>Delete Columns(s)</label></li>
    ];
  }
  if(tableItems.length)
    return <menu.context.columns><div>{basicItems}</div><div>{tableItems}</div></menu>;
  else
    return <menu.context.columns><div>{basicItems}</div></menu>;
}

