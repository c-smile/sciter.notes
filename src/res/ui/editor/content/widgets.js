
const ssEventWidgetProps = CSS.set`
   :root {
      background: color(popup-back);
      border: 1px solid color(widget-border);
      padding:4px;
      border-spacing:4px;
      font:system;
   }
   form {
      //flow:row(input,select);
      border-spacing:inherit;
   }
   form > input { display:block; }
   //form > select { display:block; height:*; overflow-y:scroll-indicator; }
   div {
      flow:horizontal;
      border-spacing:inherit;
      horizontal-align:center;
   }
   button.apply {
     display:block;
     margin:0 *;
   }
`;

// prop editor of EventWidget
class EventWidgetProps extends Element {

  eventWidget;
  htmlarea;

  constructor({widget,htmlarea}) {
    super();
    this.eventWidget = widget;
    this.htmlarea = htmlarea;
  }

  render() {

    const op = this.eventWidget ? @"Apply" : @"Insert";
    const warn = this.eventWidget?.warn ?? "";

    return <popup.widget styleset={ssEventWidgetProps}>
      <form>
        <input|calendar(date) value={this.eventWidget?.date}/>
        <div>
          <button|radio(warn) value="1m" checked={warn=="1m"}>Month</button>
          <button|radio(warn) value="1w" checked={warn=="1w"}>Week</button>
          <button|radio(warn) value="1d" checked={warn=="1d"}>Day</button>
          <button|radio(warn) value=""  checked={warn==""}>none</button>
        </div>
      </form>
      <button.apply.primary>{op}</button>
    </popup>;
/*
        <select|list(time)>
           <option value="" selected>none</option>
           <option value="0">00:00</option><option value="1">01:00</option><option value="2">02:00</option><option value="3">03:00</option>
           <option value="4">04:00</option><option value="5">05:00</option><option value="6">06:00</option><option value="7">07:00</option>
           <option value="8">08:00</option><option value="9">09:00</option><option value="10">10:00</option><option value="11">11:00</option>
           <option value="12">12:00</option><option value="13">13:00</option><option value="14">14:00</option><option value="15">15:00</option>         
           <option value="16">16:00</option><option value="17">17:00</option><option value="18">18:00</option><option value="19">19:00</option>
           <option value="20">20:00</option><option value="21">21:00</option><option value="22">22:00</option><option value="23">23:00</option>
        </select>
*/    
  }

  ["on click at button.apply"]() {
    const def = this.$("form").value;
    if(this.eventWidget) 
      this.eventWidget.update(def);
    else 
      EventWidget.insert(def,this.htmlarea);
    this.state.popup = false; 
  }
}

function warnHours(warn) {
  const [n,u] = scanf("%f%c",warn); 
  switch(u) {
    case "m": return n * 720; 
    case "w": return n * 168;
    case "d": return n * 24;
    default: n ?? 0;
  }
}

function addHours(date, hours) {
  let result = new Date(date);
  result.setHours(result.getHours() + hours);
  return result;
}

export class EventWidget extends Element {

    date;
    //time;
    warn = ""; // "" | "1m" | "1w" | "1d"

    componentDidMount() {
      //console.log("EventWidget");
      const href = this.attributes["href"];
      // event:isodate?warn=1week
      const [dateISO,params] = href.substr(6).split("?");
      if(params) {
        const list = params.split("&");
        for(let param of list) {
          let [name,value] = param.split("=");
          value = decodeURIComponent(value);
          if( name === "warn" ) {
            this.warn = value;
          }
        }
      }
      this.date = new Date(dateISO);
      this.innerText = this.date.toLocaleDateString();
      this.postEvent(new Event("event-set-change",{bubbles:true}));
    }

    componentWillUnmount() {
      this.postEvent(new Event("event-set-change",{bubbles:true}));
    }

    static create(def,htmlarea) {
      const iso = def.date.toISOString();
      const html = `<a href='event:${iso}${def.warn}'>${def.date.toLocaleDateString()}</a>`;
      htmlarea.insertHTML(html);
    }

    ["on navigate"](e) {
       // NOTE: we create the popup in context of root document so 
       //       this popup will not interfere with editing state of richtext and document in it. 
       const rootDoc = document.parentElement.ownerDocument;
       const {x,y} = this.box("border","window").pointOf(1);
       rootDoc.popup(<EventWidgetProps widget={this} />, {popupAt:7,x,y});
       return true;
    }

    get htmlarea() {
      return document.parentElement;
    }

    update(def) {
      this.date = def.date;
      this.warn = def.warn;
      let href = `event:${this.date.toISOString()}`;
      if(this.warn) href += `?warn=${this.warn}`;
      this.htmlarea.richtext.update(tctx => {
         tctx.setText(this,this.date.toLocaleDateString());
         tctx.setAttribute(this,"href",href);
         return true;
      });
      this.htmlarea.postEvent(new Event("change",{bubbles:true}));
    }

    static create(htmlarea) {
      const {x,y} = htmlarea.box("caret","window").pointOf(1);
      const rootDoc = htmlarea.ownerDocument;
      rootDoc.popup(<EventWidgetProps htmlarea={htmlarea} />, {popupAt:7,x,y});
    }

    static insert(def,htmlarea) {
      let href = `event:${def.date.toISOString()}`;
      if(def.warn) href += `?warn=${def.warn}`;
      htmlarea.insertHTML(`<a href=${href}>${def.date.toLocaleDateString()}</a>`);
      htmlarea.postEvent(new Event("change",{bubbles:true}));
    }

    get startDate() {
      const hours = warnHours(this.warn);
      return addHours(this.date, -hours);
    }
    get endDate() {
      return this.date;
    }
  }

  // check list, note uses <dir> element
  export class CheckListItem  extends Element {

    onmousedown(evt) {
      if (evt.isOnIcon) {
        this.toggle(!this.$is(".checked"));
        this.postEvent(new Event("change",{bubbles:true})); // notify document change
        return true;
      }
    } 

    toggle(onOff) {
      this.classList.toggle("checked", !this.$is(".checked"));
      this.classList.remove("mixed");
      this.updateChildrenState(onOff);
      for(let p = this.parentElement; p && p !== document; p = p.parentElement)
        if(p instanceof CheckListItem) p.checkChildrenState();
    }

    checkChildrenState()
    {
      const hasChecked = this.$(":root>dir>li.checked");
      const hasUnchecked = this.$(":root>dir>li:not(.checked");
      if( !hasChecked && !hasUnchecked ) { this.classList.remove("mixed"); }
      else if( hasChecked && !hasUnchecked ) { this.classList.toggle("checked", true); this.classList.remove("mixed");}
      else if( !hasChecked && hasUnchecked ) { this.classList.toggle("checked", false); this.classList.remove("mixed");}
      else { this.classList.add("mixed"); this.classList.remove("checked"); }
    }

    updateChildrenState( onOff ) {
      for(const child of this.$$("dir > li")) {
        child.classList.toggle("checked", onOff);
        child.classList.remove("mixed");
      }
    }

  }
