// a.k.a. "canibalize DOM"  

// called by 
//   event pastehtml $(htmlarea) (evt) 
// to normalize/preprocess pasted html: remove redundant elements and styles, etc  
// parameters: 
//   elHtml - root element (<html>) of html being pasted. This DOM element is not yet in the editable document DOM.
//            It may contain comment nodes with text "StartFragment" and "EndFragment" - they mark start/end of actual 
//            content. Do not remove them (only if you really know what you are doing).
//            The engine will remove these comments before pasting.

// returns [generator,title] if any

const inputElements = {"input":true,"textarea":true,"button":true,"widget":true,"select":true};

const requiredAttribute = { "id":true,  "name":true,  "href":true,  "src":true,  "type":true,  
                            "nowrap":true, "colspan":true, "rowspan":true, "valign":true, "align":true, "border":true};
                            // note, we deliberately exclude "class" and "style" attributes 

const prohibitedElements = { "frame":true,"iframe":true,"style":true,"script":true,"link":true, "col":true, "colgroup":true, "head":true};

const mandatoryParents = {"html":true,"body":true};
const tableParts = {"table":true,"tbody":true,"tfoot":true,"thead":true,"tr":true};
const tableCells = {"td":true,"th":true,"caption":true};

const UNKNOWN_TAG = 0;      // unknown 
const INLINE_BLOCK_TAG = 1; // <img>, <input> ...
const BLOCK_TAG = 2;        // <div>,<ul>,<p> ... 
const INLINE_TAG = 3;       // <span>,<b>,<strong> ...
const TABLE_TAG = 4;        // <table>
const TABLE_BODY_TAG = 5;   // <thead>,<tbody>,<tfoot>
const TABLE_ROW_TAG = 6;    // <tr>
const TABLE_CELL_TAG = 7;   // <td>,<th>
const INFO_TAG = 8;         // <link>,<style>,<head> ... 

const elementType = Tokenizer.elementType;

export function canonicalizeDOM(elHtml, params) {
  
  //like <meta name=Generator content="Microsoft Excel 15">
  let generator = elHtml.$("head > meta[name==generator]");
  if( generator ) generator = generator.attributes["content"];

  let title = elHtml.$("title");
  if( title ) title = title.innerText;

  function cleanContent() {

    // process attributes
    // returns true if element has no attributes

    function processAttributes(el) {
      //if( inputElements[el.tag] )
      //  return false; // do not reduce attributes in inputs
      for( let n = el.attributes.length - 1; n >= 0; --n ) {
        const {name,value} = el.attributes.item(n);
        if( requiredAttribute[name] ) 
          continue;
        delete el.attributes[name];
      }
      return el.attributes.length == 0;
    }

    // called for elements that have no attributes
    function redundant(el) {
      const tag = el.tag;
      if( tag == "span" ) return true; 
      //if( tag == "div" && !el.innerText.trim() ) return true;
      // div soup elimination
      if( tag == "div") {
        for(let node of el.childNodes)
          if(node.nodeIsText && node.data.trim())
            return false; // it has non-ws node, like <p>
        return true; // can be eliminated
      }
      return false;
    }

    var nodeStartFragment = null; 
    var nodeEndFragment = null;


    function processNode(node) 
    {
      if( node.nodeIsText ) return;

      //stdout.println("processing", node);

      if( node.nodeIsComment ) { 
        ////stdout.println("comment", node.text);
        if( node.data == "StartFragment" )
        { 
          nodeStartFragment = node;
          return;
        } 
        else if( node.data == "EndFragment" )
        {
          nodeEndFragment = node;
          return;
        }
        //stdout.println("comment removed", node.text);
        node.remove(); 
        return; 
      }
      // else it is an element
      if( node.tag == "svg" )
        return; // let's trust SVG authors

      if ( prohibitedElements[node.tag] || inputElements[node.tag] )
      {
        //stdout.println("prohibited element removed", node.tag);
        node.remove(); 
        return;
      }

      for( const child of node.childNodes ) 
        processNode(child);

      const [tm,cm,pm] = elementType(node.tag);
      // check unknown elements
      if( tm == UNKNOWN_TAG )  {
        //stdout.println("unknown element removed", node.tag);
        node.unwrapElement(); // remove the element but keep its content in the DOM
      }
      // check attributes
      else if(processAttributes(node) && redundant(node) )
      { 
        //stdout.println("redundant element removed", node.tag);
        node.unwrapElement(); // remove the element but keep its content in the DOM
      }
      
    }

    // process the DOM
    processNode(elHtml);

    // browsers are tend to put full context stack in DOM but we don't really need all that 
    if( nodeStartFragment && nodeEndFragment ) {

      let p = Node.commonParent(nodeStartFragment,nodeEndFragment);
      //stdout.println("parent removing parent", p);
      let tdseen = false;
      while(p) { 
        let rp = p;
        p = p.parentElement;
        if( !p ) break;
        const tag = rp.tag;
        if( mandatoryParents[tag] ) break;   
        if( !tdseen && tableParts[tag]) continue;
        if( tableCells[tag]  ) tdseen = true;
        //stdout.println("parent removed", tag, rp.text);
        rp.unwrapElement();
      }
    }
    //stdout.println("elHtml:", elHtml.html);
  }

  cleanContent();

  return [generator,title];
}

