
//const SS = CSS.set`
//  :root { size:*; }
//`;

import {html2html} from './remark/md.js';
import * as state from './state.js';

function colorize(plaintext) {

  // markup colorizer
  function doMarkup(tz) {
    let tagStart = null; // [node,offset]
    let tagScript = false;
    let tagScriptType = false;
    let tagStyle = false;
    let textElement;

    let tt;

    while (tt = tz.nextToken()) {
      if (tz.element != textElement) {
        textElement = tz.element;
        textElement.setAttribute("type", "markup");
      }
      // stdout.println(tt,tz.attr,tz.value);
      switch (tt) {
        case "tag-start": {
          tagStart = tz.tokenRange.start;
          const tag = tz.markupTag;
          tagScript = tag == "script";
          tagStyle = tag == "style";
        } break;
        case "tag-head-end": {
          (new Range(tagStart, tz.tokenRange.end)).highlight("tag");
          if (tagScript) {
            tz.push(tagScriptType, "</script>");
            doScript(tz, tagScriptType, true);
          }
          else if (tagStyle) {
            tz.push("text/css", "</style>");
            doStyle(tz, true);
          }
        } break;
        case "tag-end": tz.tokenRange.highlight("tag"); break;
        case "tag-attr": if (tagScript && tz.markupAttributeName == "type") tagScriptType = tz.markupAttributeValue;
          if (tz.markupAttributeName == "id") tz.tokenRange.highlight("tag-id");
          break;
      }
    }
  }

  function doIt() {
    const tz = new Tokenizer(plaintext, "text/html");
    doMarkup(tz);
  }

  doIt();

  return doIt;
}


export class SrcView extends Element {

  observers = []; // bound UI objects

  render() {

    return <section.md styleset={__DIR__ + "srcview.css#src-view"}>
      <toolbar.content>
        <button(save) @title="Save" />
        <splitter/>
        <button(undo) @title="Undo" />
        <button(redo) @title="Redo" />
        <div.filler>HTML source</div>
        <button(cancel) @title="Close" />
      </toolbar>
      <plaintext value={state.data.src} />
    </section>;
  }

  componentDidMount() {
    this.colorizeIt = colorize(this);
    for(const todisable of document.$$(".disable-on-source"))
      todisable.state.disabled = true;
  }
  componentWillUnmount() {
    for(const todisable of document.$$(".disable-on-source"))
      todisable.state.disabled = false;
  }

  setupToolbar() {

    const plaintext = this.$("plaintext");

    const AVAILABLE = 0x00; // toolbar button flags
    const ACTIVE    = 0x01;
    const DISABLED  = 0x02;

    // binds UI thing (e.g. toolbar button) with command|function to be executed onClick
    let bind = (selector, cmd, param) => {
      var uiel = this.$(selector); //assert uiel;
      if(typeof cmd == "function") {
        uiel.on("click", () => cmd.call(plaintext,"exec",param,uiel) );
        this.observers.push( () => {
          var cmdState = (current ? cmd.call(plaintext,"query",param,uiel) : 0) || 0;
          uiel.state.checked  = (cmdState & ACTIVE) != 0;
          uiel.state.disabled = (cmdState & DISABLED) != 0;
        });
      } else if(typeof cmd == "string") {
        uiel.on("click", () => plaintext.execCommand(cmd,param) );
        this.observers.push( () => {
          var cmdState = (plaintext ? plaintext.queryCommand(cmd,param) : 0) || 0;
          uiel.state.checked  = (cmdState & ACTIVE) != 0;
          uiel.state.disabled = (cmdState & DISABLED) != 0;
        });
      } else 
        throw "Bad cmd type " + cmd;
    };

    bind("button(undo)", "edit:undo");    
    bind("button(redo)", "edit:redo");

    this.updateObservers();
  }  

  updateObservers() { 
    for(let func of this.observers) func(); 
  }

  ["on statechange at plaintext"](evt) {
    this.timer(200,this.updateObservers); // NOTE: throttling updateObservers call
                                          //       to avoid frequent updates.
  }   


  get value() {
    return this.$("plaintext").value;
  } 

  ["on input"](evt) {
    this.timer(40ms, this.colorizeIt);
  }

  ["on click at button(cancel)"]() {
    state.mainViewType.value = "note";
  }

  async ["on click at button(save)"]() {
    let html = this.value;
    state.data.htmlarea.bodyHTML = html;
    state.mainViewType.value = "note";
    return true;
  }

  static async show(htmlarea) {
    let html = htmlarea.bodyHTML;
    const file = await html2html(html,true);
    state.data.src = String(file);
    state.data.htmlarea = htmlarea;
    state.mainViewType.value = "src";
    return true;
  }

}