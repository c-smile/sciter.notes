
// vectors
const burger = [[ 0.0 , 0.2, 1.0 , 0.2 ],
                [ 0.0 , 0.5, 1.0 , 0.5 ],
                [ 0.0 , 0.8, 1.0 , 0.8 ]];

const arrow =  [[ 0.5 , 0.0, 1.0 , 0.5 ],
                [ 0.0 , 0.5, 1.0 , 0.5 ],
                [ 0.5 , 1.0, 1.0 , 0.5 ]];

export class Hamburger extends Element 
{
  strokeWidth;
  strokeColor;
  duration; 

  container;
  ratio = 0.0;  // ratio : 0.0 - burger, 1.0 - arrow
  b2a;

  constructor(props) {
    super();
    this.container = props.container;
    this.duration = props.duration || 400;
  }

  render() {
    return <button.hamburger />
  }

  componentDidMount() {
    if(!this.container)
      this.container = this.$p("aside,section");
    console.assert(this.container,"Hamburger:no suitable container!");     
    this.strokeWidth = this.style.pixelsOf("stroke-width") || 1;
    this.strokeColor = this.style.colorOf("stoke") || Color.rgb(1,1,1);

    this.b2a = this.container.state.collapsed;
    this.ratio = this.b2a ? 0.0 : 1.0;
  }
                  
  paintContent(gfx) 
  {
    let [w,h] = this.state.box("dimension");
    const iw = this.style.pixelsOf("foreground-width");
    //const ih = this.style.pixelsOf("foreground-height");
    let {strokeWidth,strokeColor,ratio} = this;

    gfx.strokeWidth = strokeWidth;
    //gfx.strokeCap = "round";
    gfx.strokeStyle = strokeColor;
     
    gfx.translate(w / 2, h / 2);
    gfx.rotate( 1turn * 0.5 * ratio );
    
    for(let n = 0; n < 3; ++n) {
      const s = burger[n]; 
      const f = arrow[n];
      gfx.beginPath();
      gfx.moveTo( (Number.morph(s[0],f[0],ratio) - 0.5) * iw, (Number.morph(s[1],f[1],ratio) - 0.5) * iw );
      gfx.lineTo( (Number.morph(s[2],f[2],ratio) - 0.5) * iw, (Number.morph(s[3],f[3],ratio) - 0.5) * iw );
      gfx.stroke();
    }
  }
  
  async toggle() {  

    const step = progress => {
      this.ratio = this.b2a ? progress : (1 - progress);
      this.requestPaint();
      return true;
    };
    this.container.state.visited = true;
    if( this.container.state.collapsed ) {
      this.b2a = true;
      this.container.style.removeProperty("width"); // width is set by frameset
      this.container.state.expanded = true;
      this.container.post(new Event("expand"));
    }
    else {
      this.b2a = false;
      this.container.state.collapsed = true;
      this.container.post(new Event("collapse"));
    }
    this.state.disabled = true;
    await this.morphContent(step, {duration:this.duration});

    this.container.state.visited = false;
    this.state.disabled = false;
  }

  onclick() {
    this.toggle();
    return true;
  }


}