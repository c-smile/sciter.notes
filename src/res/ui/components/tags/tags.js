
import * as srt from "@sciter";
import {suggestionMenu} from "suggestions.js";

export class TagList extends Element {
  tags = []; // [{id:"xxx",caption:"yyy"}]
  smenu = null;

  this(props,kids) {
    this.tags = props.tags || [];
    this.suggestor = props.suggestor;
    this.placeholder = props.placeholder || "";
  }

  render() {
    return <widget styleset={__DIR__ + "tags.css#tag-list"}> 
      { this.tags.map( tag => <label.tag key={tag.id}>{tag.caption}</label> ) }
      <input|text key="input" placeholder={this.placeholder} />
    </widget>;
  }

  ["on keydown at input"](evt,input) {
    if( evt.code == "Enter" ) {
      if(this.smenu) this.smenu.hide();      
      this.addNewTag(input.value);
      input.value = "";
      return true;
    } if( evt.code == "Backspace" ) {
      if(this.smenu) this.smenu.hide();
      if(!input.value && this.tags.length) {
        this.removeTag(this.tags[this.tags.length - 1]);
        return true;
      }
    }
  }

  ["on mousedown at label"](evt,label) {
    if( evt.isOnIcon ) {
      this.removeTag(this.tags[label.elementIndex]);
      return true;
    }
  }

  // overridable, returns TagDef
  createNewTag(text) { return {id:srt.uuid(),caption:text}; }
  // overridable
  addExistingTag(tagDef) { return true; }
  // overridable
  removeExistingTag(tagDef) { return true; }

  addNewTag(text) {
    const tagDef = this.createNewTag(text);
    if(tagDef) {
      this.tags.push(tagDef) ;
      this.componentUpdate();
      this.post(new Event("change",{bubbles:true}));
    }
  }

  removeTag(tagObj) {
    if(this.smenu) this.smenu.hide();
    if(this.removeExistingTag(tagObj)) {
      this.tags = this.tags.filter( tag => tag.id !== tagObj.id );
      this.componentUpdate();
      this.post(new Event("change",{bubbles:true}));
    }
  }

  ["on change at input"](evt,input) {
    if(!this.smenu) this.smenu = suggestionMenu(input,this.suggestor);
    this.smenu.show(input.value,this.tags);
    return true;
  }

  ["on add-tag"](evt) {
    const tagDef = evt.data;
    if(this.addExistingTag(tagDef))
      this.tags.push(tagDef);
    const input = this.$("input");
    input.value = "";
    input.state.focus = true;
    this.componentUpdate();
  }


}