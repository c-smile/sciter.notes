
export class VirtualTree extends Element {
  treelines;
  #openNodes = new Set(); // items of open nodes

  constructor({treelines}) {
    super();
    this.treelines = treelines;
  }

// overridables {{{
  keyOf(item) { return item.id ?? item.text; }
  textOf(item) { return item.text; } // may return VDOM
  isNode(item) { return Array.isArray(item.items); }
  isCurrent(item) { return false; }
  kidsOf(item) { return item.items; }
  rootItems() { return []; }
// overridables }}}

  renderItem(item) {
    const key = this.keyOf(item);
    const text = this.textOf(item);
    if(this.isNode(item)) {
      const expanded = this.#openNodes.has(key) ;
      const current = this.isCurrent(item);
      let children = [];
      if(expanded) {
        const items = this.kidsOf(item);
        children = items.map(item => this.renderItem(item));
      }
      return <option.node key={key} state-data={item} expanded={expanded} state-current={current}>
        <caption>{text}</caption>
        {children}
      </option>
    } else { // terminal
      return <option key={key} state-data={item}><caption>{text}</caption></option>;
    }
  }

  render() {
    const roots = this.rootItems();
    const atts = this.treelines ? {treelines:true} : {};
    return <select|tree {atts}>{
      roots.map(item => this.renderItem(item))
    }</select>
  }

  ["on expand at option"](e,option) {
     const item = option.data;
     this.#openNodes.add(this.keyOf(item));
     option.patch(this.renderItem(item));
     return true;
  }
  ["on collapse at option"](e,option) {
     const item = option.data;
     this.#openNodes.delete(this.keyOf(item));
     option.patch(this.renderItem(item));
     return true;
  }

  ["on dblclick at option:not(:node)"](e,option) {
     const data = option.data;
     this.postEvent(new Event("item-activate",{bubbles:true,data}));
     //console.log(data);
     return true;
  }

}