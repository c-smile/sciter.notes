
import * as db from "../db/db.js";

import {VirtualTree} from "./components/virtual-tree.js";
import {Hamburger} from "./components/hamburger.js";
import {ColorSelector,InplaceEditor} from "./utils.js";
import {exportBookNotesTo,exportAllNotesTo} from "./impexp/export.js";

import * as Settings from "./boot/settings.js";

class BookView extends Element {
  book;
  constructor({book})  {
    super();
    this.book = book;
  }

  render(){
    const book = this.book;
    const atts = book.isDeleted ? {deleted:true}
               : book.isDefault ? {default:true}
               : {custom:true};
    return <option.book {atts} color={book.color || 0}>
      <label>{book.name}</label>
      <var>{book.itemsCount}</var></option>;
  }

  ["on change"]() { return true; }

  ["on contextmenu"](e) {
    const thisBook = this.book;
    const doRename = () => { InplaceEditor(this.$("label"), newText => thisBook.update({name:newText}) ); };
    const doMove = function() { db.mergeBookItems(thisBook,this.data); return true; };    
    const doSetColor = (colorId) => { thisBook.colorId = colorId; return true; };
    const doDelete = () => { db.deleteBook(thisBook); return true; }
    const doExport = () => { 
      let folder = Window.this.selectFolder();
      if(folder) { folder = URL.toPath(folder) + "/"; exportBookNotesTo(thisBook,folder); }
      return true; 
    }
    const books = db.getBooks(this.book).map(book => <li onclick={doMove} state-data={book}>{book.name}</li>);
    e.source = Element.create(<menu.context>
      <li onclick={doRename} @>Rename book</li>
      <ColorSelector caption="Book color" colorId={this.book.color} cb={doSetColor}/>
      <li@>Move notes to &hellip;
           <menu>{ books }</menu>
      </li>
      <li onclick={doExport} state-disabled={thisBook.itemsCount == 0} @>Export notes &hellip;</li>
      <li onclick={doDelete} state-disabled={thisBook.itemsCount != 0} @>Delete book</li>
    </menu>);
    return true;
  }

}

class MetaBookView extends Element {
  render() {
    return <option.book all>
      <label>All</label>
      <var>{db.totalItems()}</var></option>;
  }

  ["on contextmenu"](e) {
    const doExport = () => { 
      let folder = Window.this.selectFolder();
      if(folder) { folder = URL.toPath(folder) + "/";  exportAllNotesTo(folder);}
      return true; 
    }
    e.source = Element.create(<menu.context>
      <li onclick={doExport} state-disabled={db.totalItems() == 0} @>Export all notes &hellip;</li>
    </menu>);
    return true;
  }


}

export class BooksView extends Element {
  currentBook;

  constructor() {
    super();
    +db.signals.bookChanged;
    +db.signals.bookSetChanged;
    +db.signals.bookNotesChanged;
  }

  this({filter}) {
    if(this.currentBook !== filter.book) {
      this.currentBook = filter.book;
      this.post(() => { this.value = this.currentBook?.id ?? "*" });
    }
  }

  render() {
    const books = db.getBooks();
    return <select|list.books-view>
      <MetaBookView key="*" />
      { books.map( book => <BookView book={book} key={book.id} />) 
    }</select>;
  }

  ["on change at :root"](e) {
    const bookId = this.value;
    db.signals.resetFilter({book:db.getBook(bookId) || null});
  }

}

class TagsTree extends VirtualTree {
  filter;
  //currentTags;
  tagKind;

  this(props) {
    this.filter = props.filter;
    this.tagKind = props.kind;
    +db.signals.tagChanged;
    +db.signals.tagCreated;
    +db.signals.tagDeleted;
  }

  rootItems() { 
    const pairs = this.filter?.book ? this.filter.book.getTagsCounters(this.tagKind)
                                    : db.getTagsCounters(this.tagKind);
    return pairs;
  }

  keyOf(item) { 
    if(item.length > 2)
      return item[1].id;
    else 
      return item[0].id;
  }
  textOf(item) { 
    if( item.length > 2 ) {
      const tag = item[0];
      const tagOther = item[1];
      const count = item[2];
      return [<label>{tagOther.name} + {tag.name}</label>,<var>{count ? count : ""}</var>];
    } else  {
      const tag = item[0];
      const count = item[1];
      return [<label>{tag.name}</label>,<var>{count ? count : ""}</var>];
    }
  } 
  isNode(item) {
    if(item.length > 2) return false; // 2 levels tree
    const tag = item[0];
    return tag.hasIntersections; 
  }
  kidsOf(item) { 
    const tag = item[0];
    return tag.getIntersections(this.filter.book);
  }

  isCurrent(item) { 
    const currentTags = this.filter.tags;
    if(!currentTags || !currentTags.length )
      return false; 
    if(currentTags.length == 1)
      return currentTags[0] === item[0];
    if(currentTags.length == 2 && item.length == 3)
      return currentTags.includes(item[0]) && currentTags.includes(item[1]);
    return false;
  }

  renderItem(item) {
    const ri = super.renderItem(item);
    const tag = item[0];    
    if(tag.color) ri[1].color = "" + tag.color;
    return ri;
  }


  ["on contextmenu at :root > option"](e,option) {
    const thisTag = option.data[0];    
    const doSetColor = colorId => { thisTag.update({color:colorId}); return true; };
    const doRename = () => { 
      InplaceEditor(option.$("label"), text => thisTag.update({name:text}) );
    };
    const doDetach = () => { thisTag.removeAllItems(); return true; }
    function doMove() { thisTag.moveAllItemsTo(this.data); return true; }
    const otherTags = db.getTags(thisTag).map(tag => <li onclick={doMove} state-data={tag}>{tag.name}</li>);

    const doDelete = () => { db.removeTag(thisTag); return true; }

    e.source = Element.create(<menu.context>
      <li onclick={doRename} @>Rename tag</li>
      <ColorSelector caption="Tag color" colorId={thisTag.color} cb={doSetColor}/>
      <li @>Move all notes to tag&hellip;
         <menu.multi-column>{otherTags}</menu>
      </li>
      <li onclick={doDetach} @>Remove this tag from all notes</li>
      <li onclick={doDelete} state-disabled={thisTag.itemsCount != 0} @>Delete tag</li>
    </menu>);
    return true;
  }

}


export class TagsAttsView extends Element {
  filter;
  showTags = true;
  showAtts = true;

  this({filter}) {
    this.filter = filter;
  }

  render() {
    return <section.tags-atts-view>
      <header.tags><caption>Tags{this.showTags?":":"…"}</caption></header>
      { this.showTags ? <TagsTree.tags-list treelines={true} filter={this.filter} kind={db.Tag.KIND_CUSTOM} /> : [] }
      <header.atts><caption>Attributes{this.showAtts?":":"…"}</caption></header>
      { this.showAtts ? <TagsTree.atts-list treelines={true} filter={this.filter} kind={db.Tag.KIND_SYSTEM} /> : [] }
    </section>;
  }

  ["on click at header.tags"]() { this.componentUpdate({showTags:!this.showTags}); return true; }
  ["on click at header.atts"]() { this.componentUpdate({showAtts:!this.showAtts}); return true; }

  ["on change at select"](e,select) {
    const tagEl = select.select.currentOption;
    const tags = tagEl.data.slice(0,-1); // last element is a counter
    db.signals.updateFilter({tags});
  }
}

/*
class AddNewBook extends Element {
  componentDidMount() {
    this.timer(100ms,() => {
      this.$("input").state.focus = true;
      console.log("AAA");
    });
  }
  render() {
    return <popup.dialog styleset={__DIR__ + "main.css#popup-dialog"}>
      <label>Book name</label><input|text />
      <button>Add</button>
    </popup>;
  }
}*/

export class SideBarView extends Element {
  collapsed; // initial state

  constructor() {
    super();
    Settings.add(this);
  }

  render() {
    const filter = db.signals.filter.value;
    //const atts = this.collapsed ? { collapsed:true } : {};
    //this.collapsed = null;
    return <section.sidebar styleset={__DIR__ + "sidebar.css#sidebar"}>
        <header>
           <caption>Books:</caption>
           <button(add-new-book) />
           <Hamburger(toggle) />
        </header>
        <div.lists>
          <BooksView filter={filter}/>
          <TagsAttsView filter={filter}/>
        </div>
    </section>;
  }

  ["on click at button(add-new-book)"](e,button) {
    const nbook = new db.Book("New Book");
    db.addBook(nbook);
    db.signals.resetFilter({book:nbook});
    this.post(()=>{
      this.$(`.book[key="${nbook.id}"]`).postEvent(new Event("start-editing"));
    });
    return true;
  }

  // state persistence
  oncollapse() { Settings.saveState(); return true; }
  onexpand() { Settings.saveState(); return true; }

  store(data) {
    data["sidebar"] = this.state.collapsed ? "collapsed":"expanded";
  }

  restore(data) {
    const collapsed = data["sidebar"] === "collapsed";
    if(collapsed) this.state.collapsed = true;
  }

}