

import * as db from "../db/db.js";

import {VirtualList} from "components/virtual-select.js";
import {LRU,removeNoteFromLRU,clearLRU} from "lru.js";

function NoteView(note, isCurrent, isSelected) {

  let atts = { "book-color": note.book.color || 0 };

  if( note.hasActiveEvent ) atts.class = "active-event";

  return <option key={note.id} state-data={note}
      state-current={isCurrent}
      state-checked={isSelected} {atts}>
      <picture value={note.preview} />
      <caption>{note.caption}</caption>
  </option>;
}


class NotesList extends VirtualList {
  notes = [];

  this(props) {
    super.this(props);
    this.renderItem = NoteView; 
    this.notes = props.notes;
  }

  itemAt(at) {
      return this.notes[at];
  }

  totalItems() {
    return this.notes.length;
  }

  indexOf(item) {
    return this.notes.indexOf(item);
  }

  // overridable
  //renderItem(item, isCurrent, isSelected) {}

  renderList(items) {
    return <notes>{ items }</notes>; 
  }

  currentDidChange() {
    db.signals.currentNote.value = this.currentItem;
    return true;
  }


  ["on ^mousedown at option::marker"](e,marker) {
    if(e.target === marker) {
      const note = marker.parentElement.data;
      removeNoteFromLRU(note);
      return true;
    }
  }

}

export class LRUView extends Element {

  //this() {}

  render() {
    return <section styleset={__DIR__ + "lruview.css#view"}>
      <toolbar.content>
        <button(list-ops) @title="Operations" />
        <button(clear-list) @title="Clear list" />
      </toolbar>
      <NotesList notes={LRU.value} />
    </section>
  }

  ["on click at button(clear-list)"]() {
     clearLRU();
     return true;
  }

  ["on click at button(list-ops)"]() {
     Window.this.modal(<info>Operations on LRU list are not implementd yet</info>);
     return true;
  }




}