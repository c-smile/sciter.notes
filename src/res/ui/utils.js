

export class ColorSelector extends Element {
  colorId;
  cb;
  caption;

  constructor({colorId,cb,caption}) { 
    super();
    this.colorId = +(colorId ?? 0); 
    this.cb = cb; 
    this.caption = caption; 
  }

  render() {
    const colorId = this.colorId;
    return <section>
      <caption>{this.caption}:</caption>
      <div.row>
          <icon|fail color="0" state-checked={colorId == 0}/>
          <icon|radio-full color="1" state-checked={colorId == 1} />
          <icon|radio-full color="2" state-checked={colorId == 2} />
          <icon|radio-full color="3" state-checked={colorId == 3} />
          <icon|radio-full color="4" state-checked={colorId == 4} />
          <icon|radio-full color="5" state-checked={colorId == 5} />
          <icon|radio-full color="6" state-checked={colorId == 6} />
          <icon|radio-full color="7" state-checked={colorId == 7} />
          <icon|radio-full color="8" state-checked={colorId == 8} />
          <icon|radio-full color="9" state-checked={colorId == 9} />
      </div>
    </section>
  } 

  ["on ^mouseup at icon[color]"](e,icon) {
    const colorId = +icon.attributes["color"];
    this.cb( colorId );
    this.$p("menu").state.popup = false;
    return true;
  }
}

export function InplaceEditor(element,onSaveChange) {

  element.classList.add("editing");

  element.state.focus = true;
  element.edit.selectAll();
  element.edit.isStandalone = true;
  
  const initialText = element.value;

  element.on("keydown.editing", e => {
    switch(e.code) {
      case "Enter":
      case "NumpadEnter":
        return commitChange();
      case "Escape":
        return rollbackChange();
    }
  });

  element.on("blur.editing", e => {
    element.value = initialText;
    element.classList.remove("editing");
    return true;
  }); 

  element.on("change.editing", e => {
    return true;
  }); 

  function commitChange() {
    onSaveChange(element.value);
    element.off(".editing");
    element.classList.remove("editing");
    return true;
  }

  function rollbackChange() {
    element.value = initialText;
    element.off(".editing");
    element.classList.remove("editing");
    return true;
  }

}

export function TagView({tag,removable}) {
  return <tag><icon type={}/>{tag.name}{}</tag>;
}

export function BookView({book}) {
  const atts = book.isDeleted ? {deleted:true}
               : book.isDefault ? {default:true}
               : {custom:true};
  return <book {atts} color={book.color || 0}>{book.name}</book>;
}

