import * as db from "../db/db.js";

import {BookView} from "./utils.js";
import {TagList} from "./components/tags/tags.js";
import {View} from "./editor/view.js";
import {MDView} from "./mdview.js";
import {SrcView} from "./srcview.js";
import {WorkerFrame} from "./impexp/wframe.js";
import {platformSupportsPrinting} from "../config.js";
import {PrintView} from "./printing/print-view.js";

export class BooksView extends Element {
  note;
  constructor() {
    super();
    +db.signals.bookSetChanged;
    +db.signals.bookChanged;
    +db.signals.bookNoteChanged;
  }

  this({note}) {
    if(this.note?.book !== note?.book)
      this.post(() => { this.value = this.note?.book?.id });
    this.note = note;
  }

  render() {
    const books = db.getBooks();
    return <select|dropdown.books>{ 
      books.map( book => <option key={book.id}><BookView book={book}/></option>) 
    }</select>;
  }

  ["on change at :root"](e) {
    if(this.note) {
      const bookId = this.value;
      this.note.setBook(db.getBook(bookId));
      return true;
    }
  }

}

class TagsView extends TagList {
  note;
  this({note}) {
    this.note = note;
    const tags = note?.getCustomTags() ?? [];    
    const allCustomTags = db.getCustomTags();
    function suggestor(text,alreadySelected) {
      text = text.toLowerCase();
      return allCustomTags.filter( tag => tag.name.toLowerCase().startsWith(text) && !alreadySelected.includes(tag) );
    }
    super.this({tags,suggestor,placeholder:@"New tag"});
  }

  createNewTag(text) { // returns TagDef
    const tag = db.getTagByName(text,true/*create if not found*/);
    this.note.addTag(tag);
    return {id:tag.id,caption:tag.name};
  }

  // overridable
  addExistingTag(tagDef) {
    const tag = db.getTag(tagDef.id);
    if(tag)
      return this.note.addTag(tag);
  }

  // overridable
  removeExistingTag(tagDef) {
    const tag = db.getTag(tagDef.id);
    if(tag) {
      this.note.removeTag(tag)
      return true;
    }
  }

}

function ToolBar({note}) {

  function newNote() { 
    db.signals.currentNote.value = null; 
    return true;
  }

  function exportNote() { 
    const filter = "HTML files|*.htm;*.html|All Files|*.*";
    const fn = Window.this.selectFile({ filter, mode:"save" });
    if(fn) 
      WorkerFrame.saveNoteToFile(note, URL.toPath(fn));
    return true;
  }

  function mdView() {
    const htmlarea = document.$("htmlarea");
    MDView.show(htmlarea);
    return true;
  }

  function srcView() {
    const htmlarea = document.$("htmlarea");
    SrcView.show(htmlarea);
    return true;
  }

  function printNote() { 
    PrintView.show(note);
    return true;
  }
  
  return <toolbar.content disabled={!note}>
    <button.new-note @title="New Note — CTRL+N" accesskey="^KeyN" onclick={newNote} disabled={!db.signals.currentNote.value}/>
    <BooksView note={note} />
    <TagsView note={note} />
    { platformSupportsPrinting 
       ? <button.print-note @title="Print Note : CTRL+P" onclick={printNote} accesskey="^KeyP" disabled={!db.signals.currentNote.value}/>
       : [] }
    <button.export-note @title="Export Note : CTRL+E" onclick={exportNote} accesskey="^KeyE" disabled={!db.signals.currentNote.value}/>    
    <button.src-view @title="HTML source view : CTRL+H" onclick={srcView} accesskey="^KeyH" />
    <button.md-view @title="Markdown view : CTRL+M" onclick={mdView} accesskey="^KeyM" />
  </toolbar>;
}

export class NoteView extends Element {
  note;

  constructor() {
    super();
    +db.signals.tagChanged;
    +db.signals.tagSetChanged;
    +db.signals.noteChanged;
  }

  render() {
    this.note = db.signals.currentNote.value;
    return <section.noteview styleset={__DIR__ + "noteview.css#note-view"}>
       <ToolBar note={this.note}/>
       <View note={this.note} />
    </section>
  }

}

