import * as env from "@env";

export const APP_NAME = "Sciter.Notes";
export const DBNAME   = "sciter-notes.db";
export const SETTINGS = "sciter-notes.json";
export const VERSION = 2.0;

export const PRINCIPAL = `${env.userName()}@${env.domainName()}`;

export function dbPathFromArgs() {
  const argv = env.arguments();
  // notes.exe -db path-to-db 
  const idx = argv.indexOf("-db");
  if( idx >= 0 ) 
    return argv[idx + 1];
}

export const platformSupportsPrinting = Window.this.mediaVar("supports-printing");

export const COMMAND_SYM = env.PLATFORM == "OSX" ? "⌘" : "Ctrl+";



