
// v2 uses different format of string indexes, these needs to be rebuilt

import * as db from "../db.js";

function convertBooks() {
  for(const book of Object.values(db.root.books)) {
    let ni = db.storage.createIndex("string");
    if(book.items) {
      for(const note of book.items) {
        ni.set(note.id,note);
        console.assert(ni.get(note.id));
      }
      book.items = ni;
    }
  }  
}

function convertMain(cbprogress) {
  let ni = db.storage.createIndex("string");
  for(const note of db.root.id2item) {
    ni.set(note.id,note);
    console.assert(ni.get(note.id));
  }
  db.root.id2item = ni;
}

export function upgrade() {
   console.info("upgrading to v2 of DB...");
   convertBooks();
   convertMain(); 
   db.root.version = 2;
   db.root.needsNoteUpgrade = true;
   db.storage.commit();
   console.info("upgraded to v2 of DB");
}


