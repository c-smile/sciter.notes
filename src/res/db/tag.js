//|
//| Tag represents "tag" that can be assigned to 
//| the Item, see: addItem/removeItem. 
//|

import * as srt from '@sciter';
import * as db from 'db.js';

export class Tag {

   name;   // human readable name(caption)
   items  = null;      // index item.id -> item
   icon   = "";        // "film" 
   color  = 0;         // visual color type 0..9
   kind   = Tag.KIND_CUSTOM;

  static KIND_CUSTOM = 1;
  static KIND_SYSTEM = 2;

  constructor(tagName, iconId = "", colorId = 0, id = null)
  {
    this.id = id || srt.uuid();
    this.name   = tagName;   // human readable name(caption)
    this.items  = null;      // index item.id -> item
    this.icon   = iconId;    // "film" 
    this.color  = colorId;   // visual color type 0..9
    this.kind   = Tag.KIND_CUSTOM;
  }

  get caption() { return this.name; }
  set caption(v) { this.name = v; }
 
  addItem(item,notify = true)
  {
    if(!item.tags)
      item.tags = [this]; // first tag
    else if( item.tags.includes(this) )
      return false;
    else
      item.tags.push( this ); // append us to theirs

    if( !this.items ) this.items = db.storage.createIndex("string");
    this.items.set(item.id,item); // append their item to our collection.

    if(notify) {
      db.signals.tagChanged.send(this);
      db.signals.noteChanged.send(item);
    }

    return true;
  }

  removeItem(item)
  {
    if(!item.tags) return false;    // hmmm ... no tags, strange...
    const idx = item.tags.indexOf(this);
    if( idx >= 0 ) {
      item.tags.splice(idx,1);     // remove tag from item.
      this.items.delete(item.id);  // remove item from tag
      //db.notify("tag-removed", [item,this]);  
      db.signals.tagChanged.send(this);
      db.signals.noteChanged.send(item);
    }
    return true;
  }

  removeAllItems() {
    if(!this.items) return true;
    for(let item of this.items) {
      const idx = item.tags.indexOf(this);
      if(idx >= 0)
        item.tags.splice(idx,1); // remove tag from item.
    }
    this.items = null;
    db.signals.tagChanged.send(this);
    return true;
  }

  moveAllItemsTo(otherTag) {
    if(!this.items) return true;
    for(let item of this.items) {
      const idx = item.tags.indexOf(this);
      if(idx >= 0) item.tags.splice(idx,1); // remove tag from item.
      otherTag.addItem(item, false);
    }
    this.items = null;
    db.signals.tagChanged.send(this);
    return true;
  }

  // returns tags and item counts that contains this tag and others [[tag,count],[tag,count],...]
  getIntersections(book) {
    let map = new Map();
    for(let item of this.items) {
      if( book && (item.book !== book) ) 
        continue;
      for(const tag of item.tags) {
        if( tag === this ) continue;
        const cnt = map.get(tag) || 0;
        map.set(tag,cnt + 1);
      }
    }
    const out = [];
    for(const [tag,count] of map.entries())
      out.push([this,tag,count]);
    return out;
  }

  get hasIntersections() {
    if(!this.items) return false;
    for(const item of this.items)
      if( item.tags.length > 1 )
        return true;
    return false;
  }

  get itemsCount() { return this.items?.length ?? 0; }
  get isCustom() { return this.kind == Tag.KIND_CUSTOM; }
  get isSystem() { return this.kind == Tag.KIND_SYSTEM; }

  update(props) {
    if(props.name !== undefined) this.name = props.name;
    if(props.color !== undefined) this.color = +props.color;
    //if(props.icon !== undefined) this.icon = props.icon;
    db.signals.tagChanged.send(this);
  }


  // static method, returns standard "known" tag 
  static known(name,create = false) {

    function tag(name, iconId, colorId, id) 
    {
      if(!create)
        return null;
      const t = new Tag(name, iconId, colorId, id);
      t.kind = Tag.KIND_SYSTEM;
      db.addTag(t);
      //stdout.printf("adding tag: %V %V\n",t, db.root.tags);
      return t;
    }

    let id;
    switch(name) {
      case "web"   : id = "1"; return db.getTag(id) || tag("web","&fa-globe;",0, id);
      case "image" : id = "2"; return db.getTag(id) || tag("image","&fa-image;",0, id);
      case "word"  : id = "3"; return db.getTag(id) || tag("word","&fa-file-word-o;",0, id);
      case "excel" : id = "4"; return db.getTag(id) || tag("excel","&fa-file-excel-o;",0, id);
      case "email" : id = "5"; return db.getTag(id) || tag("email","&fa-envelope-o;",0, id);
      case "event" : id = "6"; return db.getTag(id) || tag("event","&fa-clock-o;",0, id);
      default: return null;
    }
  }
}
