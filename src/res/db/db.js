
//import * as defs from 'defs.js';
//export * from 'defs.js';

import * as srt from '@sciter';
import * as env from '@env';
import * as Storage from "@storage"; 

import {Book} from 'book.js';
import {Tag} from 'tag.js';
import {Note} from 'note.js';

export {Book} from 'book.js';
export {Tag} from 'tag.js';
export {Note} from 'note.js';

import * as signals from 'signals.js';
export {signals};

export const DELETED_BOOK_ID = "deleted-notes-book";
export const DEFAULT_BOOK_ID = "0";

export let storage = null; // exporting it to prevent deletion
export let root = null;

//|
//| check database existence
//|

export function checkDatabase(pathname)
{
  try 
  {
    let ndb = Storage.open(pathname);
    ndb.close();
    return true;
  } catch(e) {
    return false;
  }
}

//|
//| open database and initialze it if needed
//|

export function openDatabase(pathname)
{
  try {
    storage = Storage.open(pathname);
    if(!storage.root) { 
      // new db, initialize structure
      storage.root = 
        {
          id2item   :storage.createIndex("string", true), // main index, item.id -> item, unique
          date2item :storage.createIndex("date", false),  // item by date of creation index, item.cdate -> item, not unique
          tags      :{},  // map of tagid -> tag
          books     :{},  // map of bookid -> book; 
          version   :2
        };
    }
    storage.path = pathname;
    root = storage.root;
    storage.commit();    
  } catch(e) {
    //return false;
    Window.this.modal(<alert>Cannot access Notes database, seems like it is open in other application&hellip;</alert>);
    Window.this.close();
  }

  // register classes so objects of these classes will have 
  // proper prototype binding on fetch from DB. 
  storage.registerClass(Book);
  storage.registerClass(Tag);
  storage.registerClass(Note);

  document.on("beforeUnload", function() {
    if(storage) {
      purgeDeletedItems();
      root = null;
      storage.close();
      storage = null;
    }
  });

  switch(root.version) {
    case 1: srt.import("./upgrades/to2.js").upgrade(); // and fall through
    //case 2: srt.import("./upgrades/to3.js").upgrade(); // and fall through
  }

  document.timer(1s,checkPendingEvents);

  return true;
}

const lexialCollator = Intl.Collator({sensitivity:"base"});


export function needsNoteUpgrade()
{
  return root?.needsNoteUpgrade;
}

export function lexicalCompare(a,b) {
   return lexialCollator.compare(a,b);
}


/*export function notify(eventName, data) {
  Window.post(new Event(eventName, {data}));
}

export function request(eventName, data) {
  Window.send(new Event(eventName, {data}));
}*/

export function currentBook() {
  return signals.filter.value.book;
}


export function checkPendingEvents() {
  let eventTag = Tag.known("event");
  let counter = 0;
  if(eventTag?.items) for(const note of eventTag.items) {
    if(note.hasActiveEvent)
      ++counter;
  }
  signals.pendingEventsCounter.value = counter;
}

//|
//| db.addItem method - add item (note) to the db with optional list of tags
//| tags can be an array of strings (full path names of tags) or
//| objects of class Tag
//| 
export function addItem(item, tags = null) 
{
  const now = new Date(); 

  if(!item.mdate) item.mdate = now; // modification date-time
  if(!item.cdate) item.cdate = now; // creation date-time
  
  item.id = item.id || srt.uuid();  // each item has an id - global unique identifier 
  const book = currentBook() || defaultBook();
  book.addItem(item);
 
  if( item.tags && (tags === null)) tags = item.tags; 
  item.tags = [];
  
  root.id2item.set(item.id,item);
  root.date2item.set(item.cdate,item);  
    
  //stdout.printf("\noriginal item:%V",item);
  if(tags)
  {
    //stdout.printf("tags:%V\n",tags);  
    for( let tag of tags )
    {
      if( typeof tag == "string" )
        tag = getTag(tag,true);
      if( tag instanceof Tag )
        tag.addItem(item);
    }
  }
  signals.bookNotesChanged.value = book;
  return item;
}

export function getItem(id)
{
  return root.id2item.get(id);
}

export function totalItems()
{
  return root.id2item.length;
}


// import item with the check for existence
export function importItem(item, book, tags ) 
{
  var action; // action taken

  var existing = root.id2item[item.id];
  if( !existing ) {
    existing = addItem(item);
    action = "created";
  }
  // we have two notes sharing same note id (that is GUID)  
  else if(existing.html == item.html) {  
    action = "skipped";
  }
  else //if(existing.html != item.html) 
  {
    var ourDate = existing.mdate.valueOf();
    var theirDate = item.mdate.valueOf();
    var modified; 
    //debug dates: existing, item , existing.mdate, item.mdate;
    if( ourDate <= theirDate ) {
      //debug theirsontop: item;
      modified = existing.mergeContent(item,"theirs-on-top");
    }
    else {
      //debug oursontop: item;
      modified = existing.mergeContent(item,"ours-on-top"); 
    }
    action = modified ? "updated" : "skipped";
  } 

  assignItemToBook(existing, book);

  for(const tag of tags)
    if(!existing.containsTags(tag))
      assignTagToItem(existing,tag);

  return [existing,action];
}

export function purgeDeletedItems() {

  const delBook = getDeletedBook();

  if(delBook.items) {  
    for(const item of delBook.items ) {
      root.id2item.delete(item.id);
      root.date2item.delete(item.cdate,item);
      // remove it from tags
      if(item.tags) {
        for( const tag of item.tags )
          tag.items.delete(item.id);     // remove item from tag
        item.tags = null;
      }
    }
    delBook.items.clear();
    console.assert(delBook.items.length == 0);
  }
}

export function addTag(tag) 
{
  root.tags[tag.id] = tag;
  signals.tagSetChanged.send(tag);
  return tag;
}

export function getTag(tagId)
{
  return root.tags[tagId];
}

export function getTags(except) {
  let list = Object.values(root.tags);
  if(except) list = list.filter(tag => tag !== except);
  list.sort( (a,b) => lexicalCompare(a.name,b.name) );
  return list;
}

export function getCustomTags() {
  return getTags().filter( tag => tag.isCustom);
}

export function getTagsCounters(tagKind) { 
  let r = getTags().filter( tag => tag.kind == tagKind)
                   .map( tag => [tag,tag.itemsCount] );
  r.sort( (a,b) => lexicalCompare(a[0].name,b[0].name) );
  return r || [];
}

// 
export function getTagByName(name,create = false)
{
  let tag = getTags().find(t => t.name.toLowerCase() === name.toLowerCase());
  if( tag ) return tag;
  
  if( create )
    return addTag(new Tag(name));     

  return null;
}

export function getUniqueTagName(name) {
  if(!getTagByName(name,false))
    return name;
  for(let counter = 2;counter < 10;++counter) {
    let n = name + counter;
    if(!getTagByName(n,false))
      return n;
  }
  console.assert(false);
  return name;
}

// assign tag to the item
export function assignTagToItem(item,tag) {
  return tag.addItem(item);
}

// remove tag from the item
export function removeTagFromItem(item,tag) 
{
  //notify("note-tags-changed",item);
  return tag.removeItem(item);
}

export function removeTag(tag)
{
  if( tag.itemsCount )
    return false;
  delete root.tags[tag.id];
  signals.tagDeleted.value = tag; 
  if(signals.filter.value.tags) {
    let tags = signals.filter.value.tags.filter(t => t !== tag);
    signals.updateFilter({tags: tags.length ? tags : null});
  }
  return true;
}


// enumerate items in creation sequence order, returns generator 

export function* matchingItems(filter)
{
  let startDate = new Date(1000,1,1);
  let endDate = new Date(3000,1,1);

  if( !filter.tags && !filter.words && !filter.book  && !filter.events)
  {
    for(let it of root.date2item.select(startDate,endDate,false)) {
      if( !it.book.isDeleted )
        yield it;
    }
  }
  /*else if(filter.events)
  {
    let out = [];
    let now = (new Date()).valueOf();
    let eventTag = Tag.known("event");
    if(eventTag) for(let it of eventTag.items)
      if( it.matches(filter) ) {
        let starts = it.startDate;
        if( !starts ) continue;
        out.push(it);
      }
    out.sort((note1, note2) => note1.startDate.valueOf() - note2.startDate.valueOf());
    for(let it of out)
      yield it;
  }*/ 
  else 
  {
    for(let it of root.date2item.select(startDate,endDate,false))
      if( it.matches(filter) )
        yield it;
  }
}

export function eventsForYear(year) {  // returns Map {"yyyy-m-d": [list of notes for the day]}
  const filter = signals.filter.value;
  const map = new Map();
  let now = (new Date()).valueOf();
  let eventTag = Tag.known("event");
  if(eventTag.items) for(let it of eventTag.items) {
    const starts = it.startDate;
    if( !starts ) continue;
    const ends = it.endDate;    
    if(ends.getFullYear() < year) continue;
    if(starts.getFullYear() > year) continue;
    if( it.matches(filter) ) {
      //out.push(it);
      const key = `${ends.getFullYear()}-${ends.getMonth()}-${ends.getDate()}`;
      let list = map.get(key);
      if( list ) list.push(it); else list = [it];
      map.set( key,list );
    }
  }
  return map;
}

export function* bookNotes(book) {
  var startDate = new Date(1000,1,1);
  var endDate = new Date(3000,1,1);
  for(const it of this.root.date2item.select(startDate,endDate,false))
    if( it.book === book )
      yield it;
}


export function* selectTags(tagKind) {
  for(const t of Object.values(root.tags))
    if( t.kind == tagKind )
      yield t;
}

export function addBook(book) 
{
  root.books[book.id] = book;
  signals.bookSetChanged.value = book;
  return book;   
}

export function getBook(bookId)
{
  return root.books[bookId];
}

export function getBooks(except) 
{
  let defbook = defaultBook();

  let out = [];
  for( let book of Object.values(root.books)) {
    if( book.isDeleted ) continue;
    if( book === except ) continue;
    out.push(book);
  }
  out.sort( (a,b) => {
    if( a === defbook ) return -2;
    if( b === defbook ) return 2;
    return lexicalCompare(a.name,b.name);
  });
  const delBook = getDeletedBook();
  if(delBook !== except)
    out.push(delBook);
  return out;
}

// get book holding deleted notes
export function getDeletedBook()
{
  var delb = root.books[DELETED_BOOK_ID];
  if( !delb )
    root.books[DELETED_BOOK_ID] = delb = new Book(@"Deleted notes", DELETED_BOOK_ID);
  return delb;
}

export function getBookByName(name,create = false)
{
  for(let book of Object.values(root.books))
    if( book.name == name )
      return book;
  if( create )
    return addBook(new Book(name));     
  return null;
}

export function getUniqueBookName(name) {
  if(!getBookByName(name,false))
    return name;
  for(var counter = 2;counter < 10;++counter) {
    var n = name + counter;
    if(!getBookByName(n,false))
      return n;
  }
  console.assert(false);
  return name;
}

export function getBookItemsCount(book /*null - total notes in all books*/) {
  return book ? book.itemsCount : root.date2item.length;
}

// returns default book
export function defaultBook() {
  const id = DEFAULT_BOOK_ID;

  let book = root.books[id]; 
  if( !book ) {
     book = new Book(env.userName() + "'s", id); 
     root.books[id] = book;
  }
  return book;  
}

export function assignItemToBook(item, book)
{
  const oldBook = item.book;   
  if( oldBook === book ) {
    return;
  }
  if(oldBook)
    removeItemFromBook(item,oldBook);

  item.book = book;

  if( !book.items ) book.items = storage.createIndex("string");
  book.items.set(item.id, item); // append their item to our collection.

  signals.bookNotesChanged.send(book);
  signals.noteChanged.send(item);
}

export function removeItemFromBook(item, book)
{
  console.assert(book === item.book);
  book.items.delete(item.id); // remove item from book
}

export function deleteBook(book, bookToMoveItemsInto = null) {
  if( book.items?.length )
    mergeBookItems(book, bookToMoveItemsInto || db.defaultBook());
  delete root.books[book.id];
  signals.bookSetChanged.value = book;
  if(signals.filter.value.book === book) 
    signals.updateFilter({book:null});
}

export function mergeBookItems(fromBook, toBook) {
  let cnote = signals.currentNote.value;
  if( !toBook.items ) toBook.items = storage.createIndex("string");
  for( const item of fromBook.items ) {
    toBook.items.set(item.id,item); 
    item.book = toBook;
    if( cnote === item )
      signals.bookNotesChanged.value = toBook;
  }
  fromBook.items = null;
  signals.bookNotesChanged.value = fromBook;
}

