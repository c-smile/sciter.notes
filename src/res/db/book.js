//|
//| Book is a container of notes (a.k.a. "items")
//|

//include "book-mapping.tis";
import * as srt from '@sciter';

import * as db from 'db.js';

const lexicalCompare = db.lexicalCompare;

export class Book
{
  id;
  name;         // human readable name(caption)
  items = null; // index item.id -> item
  color = 0;    // visual color type 0..9

  constructor(bookName, id = null)
  {
    this.id = id || srt.uuid();
    this.name   = bookName;  
    this.items  = null;      
    //this.color  = colorId;   
  }

  static get(id) {
    return db.getBook(id);
  }
 
  addItem(item)
  {
    db.assignItemToBook(item,this);
  }

  removeItem(item)
  {
    db.removeItemFromBook(item,this);
  }

  // returns array of [tag,notescount] 
  getTagsCounters(tagKind) { 
    if(!this.items) return [];
    const map = new Map();
    for(const item of this.items) 
    {
      if(item.tags) for(const tag of item.tags) 
      {
        if( tag.kind != tagKind ) continue;
        const cnt = map.get(tag) || 0;
        map.set(tag,cnt + 1);
      }
    }
    let r = Array.from(map.entries());
        r.sort( (a,b) => lexicalCompare(a[0].name,b[0].name) );
    return r;
  }
  
  get itemsCount() { 
    return this.items?.length || 0; 
  }

  update(props) {
    if(props.name !== undefined) this.name = props.name;
    if(props.color !== undefined) this.color = +props.color;
    //if(props.icon !== undefined) this.icon = props.icon;
    db.signals.bookChanged.send(this);
  }

  get isDefault() { return this.id === db.DEFAULT_BOOK_ID; }
  get isDeleted() { return this.id === db.DELETED_BOOK_ID; }

  get colorId() { return this.color; } 
  set colorId(id) { this.color = id; db.signals.bookChanged.send(this); } 

/*
  addMapping(folderPath) {
    this.mappedTo = folderPath;
    db.notify("book-mapping-added",this);
  }

  removeMapping() {
    this.mappedTo = null;
    db.notify("book-mapping-removed", this);
  }
*/  

}

