
const {signal} = Reactor;

export const bookSetChanged = signal(null);  // added / deleted 
export const bookChanged = signal(null);
export const bookNotesChanged = signal(null); // notes added removed from the book 

export const currentNote = signal(null);
export const noteChanged = signal(null);

export const tagChanged = signal(null);
export const tagSetChanged = signal(null); // tag added or deleted

export const pendingEventsCounter = signal(0); // tag added or deleted

export const filter = signal({book:null}); // current notes filter
                                           //{book,words,tags,}

export function updateFilter(obj) {
  filter.value = Object.assign({},filter.value,obj);
}

export function resetFilter(obj) {
  filter.value = obj;
}