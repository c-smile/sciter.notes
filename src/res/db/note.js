//|
//| Item is a note that could be of various types 
//|

//include "../libs/utils.tis";

import * as srt from '@sciter';
import * as env from '@env';
import * as db from 'db.js';
import {Tag} from 'tag.js';
import * as config from '../config.js';

const PRINCIPAL = `${env.userName()}@${env.domainName()}`;

function daysBetween (date1, date2) {
    let difference = date1.getTime() - date2.getTime();
    return Math.ceil(difference / (1000 * 3600 * 24));
}

export class Note 
{
  id = null;
  cdate = null;
  mdate = null;  
  kind = null;
  book = null;  
  tags = [];  
  html = null;
  text = null;
  caption = null;
  resources = null;
  lock = undefined;
  preview = null; // preview image bytes
  cwho; // created by whom    
  mwho; // modified by whom    
  meta; // various metadata : 
           // { 
           //   urlFrom : ...pasted from this url ... 
           // } 
  history; // history records, array of 
           // { mdate: Date, 
           //   mwho: String - whos version is that
           //   html: Bytes - content }

  constructor(itemKind = "html") {
    const date = new Date();
    this.id = srt.uuid();
    this.cdate = date;
    this.mdate = date;  
    this.kind = itemKind;
    this.cwho = PRINCIPAL;
  }

  /*function setContent(html, text, caption, preview) 
  {
    this.html = html;
    this.text = text;
    this.caption = caption;
    this.preview = preview;
    this.mdate = new Date();  
    this.mwho = PRINCIPAL;  
    
    self.postEvent("note-updated",this);    
  }*/

  static get(id) {
    return db.getItem(id);
  }

  get url() {
    return `note:${this.id}`;
  }

  get isNew() {
    return !this.book;
  }

  update(props) {
    // use with care - no check for field names!
    let changes = false;
    let contentChanges = false;
    for(const [key,val] of Object.entries(props)) 
      if( this[key] != val ) {
        changes = true;
        if(key == "html")
          contentChanges = true;
      }
    
    if(!changes)
      return;

    const now = new Date();      
    this.mdate = now;
    this.mwho = PRINCIPAL; 

    /*if(contentChanges) {
      if((daysBetween(this.mdate,now) >= 1) || (this.mwho != PRINCIPAL))
        this.pushToHistory(this);
    }*/

    for(const [key,val] of Object.entries(props)) 
      this[key] = val;

    db.signals.noteChanged.send(this);
    db.storage.commit(); 
  } 

  setBook(book) {
    db.assignItemToBook(this, book);
  }
 

  setMeta(params) 
  {
    let changes = false;
    if( !this.meta ) this.meta = {};
    for(let [key,val] of Object.entries(params)) {
      if( this.meta[key] != val ) {
        this.meta[key] = val;
        changes = true;
      }
    }
    if(changes) {
      this.mdate = new Date();
      this.mwho = PRINCIPAL;  
      db.signals.noteChanged.send(this);
    }
  }  
  
  getMeta(name) {
    return this.meta ? this.meta[name] : undefined;
  }

  setLock(onOff) {
    if(this.lock !== onOff) {
      this.lock = onOff;
      db.notify("note-lock-changed", this);
    }
  }

  addResource(url, mime, bytes)
  {
    if( !this.resources) 
      this.resources = {};
    this.resources[url] = [bytes,mime];
  }

  getResource(url)
  {
    if( this.resources) {
      let r = this.resources[url];
      if( Array.isArray(r))
        return r;
    }
    return [null,null];
  }

  containsTags(tags) {
    const thistags = this.tags;
    if( !thistags )
      return false;
    if( Array.isArray(tags) ) {
      for( let t of tags)
        if(!thistags.includes(t)) 
            return false;
    } else if(typeof tags == "string") {
      if(!thistags.includes(tags)) 
            return false;
    }
    return true;
  }

  matches({tags,words,book}) {
    if( tags && !this.containsTags(tags)) return false;
    if( book ) {
      if(this.book !== book) return false;
    } else { // !book
       if( this.book.isDeleted ) // we are not showing notes from deleted book for "all notes"
         return false;  
    }
    if( words ) {
      var text = (this.text || "").toLowerCase();
      for(const word of words) 
        if(word && (text.indexOf(word) < 0))
          return false;
    }
    return true;
  }

  addTag(tag) { return db.assignTagToItem(this,tag); }

  removeTag(tag) { return db.removeTagFromItem(this,tag); }

  getCustomTags() {
    if(this.tags)
      return this.tags.filter( tag => tag.isCustom);
  }

  getTags() {
    return this.tags;
  }
  
  get createdBy() {
    return this.cwho || PRINCIPAL;
  }
  set createdBy(v) {
    this.cwho = v;
  }

  get modifiedBy() {
    return this.mwho || this.cwho || PRINCIPAL;
  }

  set modifiedBy(v) {
    this.mwho = v;
  }
/*
  pushToHistory(from)
  {
    if(!this.history) 
      this.history = [];

    if( this.history.last && this.history.last.html == from.html)
      return false;

    //db.notify("note-history-changed",this);

    // collapse all changes that happened in one day
    const last = this.history.last
    if( last ) {
      if(last.mwho == from.modifiedBy && daysBetween(last.mdate,from.mdate) < 1) 
      last.mdate = from.mdate;
      last.html = from.html;
      return true;
    }

    this.history.push({
      mdate: from.mdate,
      mwho: from.modifiedBy,
      html: from.html
    });
    if(this.history.length > 8) // we keep only this number of history items
      this.history.shift();

    return true;
  }
*/
/*  
  mergeContent(other, how) {
    //debug log: currentNote === this, currentNote, this;
    //if( currentNote === this ) {
    //  // do it visually
    //  if(NoteView.current.mergeNote(other, how))
    //    return true;
    //}
    db.request("note-save",this);
    // how: #their-on-top or #ours-on-top
    if( how == "theirs-on-top" ) {
      if(!this.pushToHistory(this))
        return false;
      this.mdate = other.mdate;
      this.mwho = other.modifiedBy;
      this.html = other.html;
    }
    else if( how == "ours-on-top" ) 
      if(!this.pushToHistory(other))
        return false;
    db.notify("note-updated",this);
    return true;        
  }
*/  

  // events is an array 
  // [{ start: Date, end: Date }, ... ]
  setEvents( events ) 
  {
    if( events && (events.length == 0)) 
      events = null;
    this.setMeta({events}); 
    if( events ) {
      this.addTag(Tag.known("event",true));
      //db.notify("events-changed",this);
    }
    else {
      this.removeTag(Tag.known("event",true));
      //db.notify("events-changed",this);
    }
    document.timer(1s,db.checkPendingEvents);
  }

  get startDate() {
    let events = this.getMeta("events");
    if(!events || events.length == 0)
      return null;
    return events[0].start;
  }

  get endDate() 
  {
    let events = this.getMeta("events");
    if(!events) return null;
    let max = null;
    for(const edef of events) {
      const e = edef.end;
      if( !max || e > max ) 
        max = e;
    }
    return max;
  }

  get activeStartDate() {
    const events = this.getMeta("events");
    if(!events)
      return null;
    var now = new Date();
    for(const edef of events) {
      if( edef.end < now ) continue; // ended already
        return edef.start;
    }
    return null;
  }

  // true if note contains active event (the one that has warn date < now)
  get hasActiveEvent() {
    const dt = this.activeStartDate;
    return dt && (dt <= (new Date()));
  }

  // move the note to the deleted book
  moveToDeletedBook() {
    const delBook = db.getDeletedBook();
    db.assignItemToBook(this, delBook);
  }

  dataForClipboard() {
    return { 
      json: { noteid: this.id },
      html: `<a href="note:${this.id}">${this.caption}</a>`,
      text: `[${this.caption}](note:${this.id})`
    }
  }

  onrequest(rq) { 
    let [data,mime] = this.getResource(rq.url);
    if( data )
      rq.fulfill(data,mime);
  } 

  onrequestresponse(rs) {
    if(rs.ok && rs.request.context == "image") {
      if(!rs.url.startsWith("data:")) 
        this.addResource(rs.request.url, rs.mimeType, rs.arrayBuffer());
      Tag.known("image",true).addItem(this);
      return true;
    }
  }

  static createNew() {
    const note = new Note();
    note.html = encode("<html><body><p></p></body></html>");
    note.text = "";
    db.addItem(note);
    return note;
  }

}