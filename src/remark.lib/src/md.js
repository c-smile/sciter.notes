import { remarkDefinitionList, defListHastHandlers } from 'remark-definition-list';
import { unified } from 'unified';
import remarkGfm from 'remark-gfm';
import remarkParse from 'remark-parse';
import remarkDirective from 'remark-directive';
import remarkRehype from 'remark-rehype';
import remarkStringify from 'remark-stringify';

import rehypeRemark from 'rehype-remark';
import rehypeStringify from 'rehype-stringify';
import rehypeParse from 'rehype-parse';
import rehypeFormat from 'rehype-format';

export async function md2html(md) {
  return unified()
    .use(remarkParse)
    .use(remarkGfm)
    .use(remarkDirective)
    .use(remarkDefinitionList)
    .use(remarkRehype, {
      handlers: { ...defListHastHandlers }
    })
    .use(rehypeStringify)
    .process(md);
}

export async function html2md(html) {
  return unified()
    .use(rehypeParse)
    .use(rehypeRemark, {
      handlers: {...defListHastHandlers}
    })
    .use(remarkGfm)
    .use(remarkDirective)
    .use(remarkDefinitionList)
    //.use(remarkRehype, {
    //  handlers: { ...defListHastHandlers }
    //})
    .use(remarkStringify,{fences:true,listItemIndent:"one"})
    .process(html);
}

// pretty formatter
export async function html2html(html, isFragment) {

  return unified()
    .use(rehypeParse,{fragment: isFragment})
    .use(rehypeFormat)
    .use(rehypeStringify)
    .process(html);
}

