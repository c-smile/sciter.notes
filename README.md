# Sciter.Notes

![Sciter.Notes](screenshot.png)

## Description

Sciter.Notes is a personal notes management application. Main features:

* Notes are stored strictly in local DB. 
* Notes are stored in DB as HTML documents. 
* Notes are editable in WYSIWYG HTML editor or as a Markdown document that is transparently converted to HTML and stored in that form.
* Application supports as Portable as Permanent operation modes. 
  * In Portable mode executable as DB and sciter-notes.json can be placed on memory stick.

Sciter.Notes executable is meant to be assembled by [Sciter.Quark](https://quark.sciter.com/) from Sciter SDK of at least version 5.0.2.9.

It also can be run in usciter demo browser for development and debugging. Simply open main.htm file in usciter.


## Installation

Steps to create your own executable:
   
1. Download [Sciter SDK](https://gitlab.com/sciter-engine/sciter-js-sdk);
2. Run [sdk.js/bin/quark](https://gitlab.com/sciter-engine/sciter-js-sdk/-/tree/main/bin/quark) batch specific to your platform.
3. Configure Quark Project similar to this:
   ![Quark project](screenshot-quark.png)
4. Press _Assemble_ button. You should have your own Sciter.Notes now.

Alternatively place scapp.exe in the same folder as src/main.htm and run scapp exe without parameters.

## Usage

TBD...

## Support

TBD...

## Roadmap

TBD...

## Contributing

TBD...

## License

New BSD Licens:

Copyright 2023, Andrew Fedoniouk.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## Project status

Beta!
